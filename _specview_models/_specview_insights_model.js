const mongoose = require('mongoose')
const Schema = mongoose.Schema

const insightSchema = new Schema(
    {
        email: String,
        platform:String,
        insight: { }
    }
)

module.exports={
    insightModel:mongoose.model('insight',insightSchema)
}