const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const feedbackFormSchema = new Schema(

    {
        clientId:String,
        email: { type: String, required: true },
        feedbackFormData: []
    }
)

module.exports = {
    feedbackFormModel: mongoose.model('feedbackform', feedbackFormSchema)
}