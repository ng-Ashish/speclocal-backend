const mongoose = require('mongoose')
const Schema = mongoose.Schema

const campaignSchema = new Schema(
    {
        id:Number,
        email: String,
        client_id:String,
        message: String,
        customers: [
            {
                customer_id:String,
                name:String,
                email:String,
                phoneno:Number,
                occupation:String,
                birthdate:String,
                anniversary:String,
                address:String,
		dialCode:String
            }],
        createdat: { type: Date, default: Date.now }
    }
)

module.exports = {
    campaignModel: mongoose.model('campaign', campaignSchema)
}