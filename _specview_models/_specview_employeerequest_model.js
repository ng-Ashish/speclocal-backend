const mongoose = require('mongoose')
const Schema = mongoose.Schema

const employeeRequest = new Schema(
    {
        email: String,
        noOfEmployees: Number,
        status: { type: Boolean, default: false },
        req_date: { type: Date, default: Date.now },
        approve_date: { type: Date, default: null },
	    lastname : String,
	    firstname : String,
	    contactno : Number	
    }
)


module.exports={
    employeerequestModel:mongoose.model('employeerequest',employeeRequest)
}