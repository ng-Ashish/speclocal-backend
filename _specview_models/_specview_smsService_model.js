const mongoose= require('mongoose')
const Schema = mongoose.Schema
const smsServiceSchema= new Schema(
    {
        email:String,
       // smsService:[
           // {
               birthday:{type:Boolean , default:false},
               anniversary:{type:Boolean , default:false},
	       smscampaign :{type:Boolean , default:false},
           // }],
            lastname : {type :String},
            firstname : {type :String},
            contactno : {type :Number},
            createdat:{type:Date , default:Date.now},
            approve_date: { type: Date, default: null },
            status: { type: Boolean, default: false },	
    }
)

module.exports={
    smsServiceModel:mongoose.model('smsService',smsServiceSchema)
}