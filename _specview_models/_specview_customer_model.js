const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const customerSchema = new Schema(
    {
        client_id: String,
        email: String,
        customers: [
            {
                id: String,
                name: String,
                phoneno: String,
                email: String,
                occupation: String,
                birthdate: String,
                anniversary: String,
                address: String,
		dialCode:String,
                is_feedback: { type: Boolean, default: false },
                campaignmsg: { type: Boolean, default: false }
            }
        ],
        status: { type: Boolean, default: false },
        createdat: { type: Date, default: Date.now },
        updatedat: { type: Date, default: Date.now }
    }
)

module.exports = {
    customerModel: mongoose.model('customer', customerSchema)
}