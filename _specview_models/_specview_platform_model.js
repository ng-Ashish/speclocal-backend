const mongoose = require('mongoose')
const Schema = mongoose.Schema


const platformSchema = new Schema({
    businessServiceType : { type : String },
    platformName : { type : String },
    
    platformLink : { type : String }
})

module.exports = {
    platformModel : mongoose.model('platforms', platformSchema)
}