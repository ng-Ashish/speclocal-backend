const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const googleSchema = new Schema(
    {
        email: { type: String },
        connected_with_google: { type: Boolean },
        accounts: [],
        locations: [],
        linked_location: {},
        total_review_count: { type: Number },
        average_rating: { type: Number , default:0},
        refresh_token: String,
        linked_account: {},
        reviews: [],
        status: { type: Boolean, default: true }
    }
)

module.exports = {
    googleModel: mongoose.model('google', googleSchema)
}