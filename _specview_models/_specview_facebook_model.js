const mongoose = require('mongoose')
const Schema = mongoose.Schema

const facebookSchema = new Schema(
    {
        email: { type: String },
        connected_with_facebook: { type: Boolean },
        account: {},
        page_access_tokens: [],
        linked_page: {},
        reviews: [],
        status: { type: Boolean, default: true }
    }
)

module.exports = {
    facebookModel: mongoose.model('facebook', facebookSchema)
}