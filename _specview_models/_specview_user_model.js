const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const crypto = require('crypto')


const userSchema = new Schema(
    {
        firstname: { type: String },
        lastname: { type: String },
        email: { type: String },
        adminemail: { type: String, default: '' },
        password: { type: String },
        contactno: { type: String },
        businessname: { type: String, },
        role: { type: String },
        msgcredit: { type: Number, default: 100 },
        status: { type: Boolean, default: true },
        //new
        platformname: [],
        smsserviceid: { type: String, default: 'SPECST' },
        smscampaining: { type: Boolean, default: false },
        birthday: { type: Boolean, default: false },
        anniversary: { type: Boolean, default: false },
        smsService: [{
            birthday: { type: Boolean, default: false },
            anniversary: { type: Boolean, default: false },
        }],
        dob: { type: String, default: '' },
        gender: { type: String, default: '' },
        businessaddress: { type: String, default: '' },
        fbpageurl: { type: String, default: '' },
        googlepageurl: { type: String, default: '' },
        busuinessservicetype: { type: String, default: '' },
        address: { type: String, default: '' },
        createdat: { type: Date, default: Date.now },
        updatedat: { type: Date, default: Date.now },
        noOfEmployees: { type: Number, default: 1 },

        route: { type: String },
        autorenewal: { type: Boolean },
        renewvalamt: { type: Number, default: 100 }

    }
);

// userSchema.methods.setPassword=function(password){
//     this.salt=crypto.randomBytes(16).toString('hex')
//     this.hash=crypto.pbkdf2Sync(password,this.salt,1000,64,`sha512`).toString(`hex`)
// }

// userSchema.methods.validPassword =function(password){
//     var hash=crypto.pbkdf2Sync(password,this.salt,1000,64,`sha512`).toString(`hex`)
//     return this.hash===hash
// }
userSchema.methods.encrypt = function (password) {
    var cipher = crypto.createCipher('aes-256-cbc', 'd6F3Efeq')
    var crypted = cipher.update(password, 'utf8', 'hex')
    crypted += cipher.final('hex');
    return crypted;
}
userSchema.methods.decrypt = function (password) {
    var decipher = crypto.createDecipher('aes-256-cbc', 'd6F3Efeq')
    var dec = decipher.update(password, 'hex', 'utf8')
    dec += decipher.final('utf8');
    return dec;
}


// hide password while returning user data
//userSchema.methods.toJSON = function () {
//var obj = this.toObject();
//  delete obj.password;
// delete obj.salt;
//return obj;
//}


module.exports = {
    userModel: mongoose.model('user', userSchema)
}