const mongoose = require('mongoose')
const Schema = mongoose.Schema

const msgtemplateSchema = new Schema(
    {
        client_id : String,
        templatename:String,
        messagebody:String,
        createdat:{type:Date ,default:Date.now}
    }
)

module.exports={
    msgtemplateModel:mongoose.model('campaignmessagetemplate',msgtemplateSchema)
}