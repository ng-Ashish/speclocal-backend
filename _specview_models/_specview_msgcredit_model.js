const mongoose = require('mongoose')
const Schema = mongoose.Schema

const msgcreditSchema = new Schema(
    {
        email: String,
        msgcredit: Number,
        status: { type: Boolean, default: false },
        req_date: { type: Date, default: Date.now },
        approve_date: { type: Date, default: null },
	lastname : String,
	firstname : String,
	contactno : Number	
    }
)

module.exports = {
    msgcreditModel: mongoose.model('msgcredit', msgcreditSchema)
}
