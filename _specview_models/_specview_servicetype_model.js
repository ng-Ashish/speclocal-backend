const mongoose = require('mongoose')
const Schema = mongoose.Schema



const serviceTypeSchema = new Schema({
    businessServiceType : {type : String}
})

module.exports={
    serviceTypeModel : mongoose.model( 'service_type' , serviceTypeSchema)
}