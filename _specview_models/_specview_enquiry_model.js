const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const enquirySchema = new Schema(
    {
        fullname:String,
        email: String,        
        contactno : Number,
        businessname : String,
        requirements : String,
        contacttype : String,        
        createdon: { type: Date, default: Date.now }        
    }
)

module.exports = {
    enquiryModel: mongoose.model('enquiry', enquirySchema)
}