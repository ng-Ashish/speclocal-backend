const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const invitationSchema = new Schema(
    {
        email: String,
        goal: { type: Number, default: 10 },
        quick_invite_count: {type: Number, default: 0},
	invitation:{type:Number,default: 10},
	adminemail :String,
	createdate: { type: Date, default: Date},
        updatedate: { type: Date, default: Date}
    }
)

module.exports = {
    inviteModel: mongoose.model('invite', invitationSchema)
}