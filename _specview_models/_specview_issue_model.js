const mongoose = require('mongoose')
const Schema = mongoose.Schema

const issueSchema = new Schema(
    {
        email: String,
        issue_related_to: String,
        issue_description: String,
        issue_status: Boolean,
        createdat: { type: Date, default: Date.now },
        updatedat: { type: Date, default: Date.now }
    }
)

module.exports = {
    issueModel:mongoose.model('issue', issueSchema)
}