const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const zomatoSchema = new Schema(
    {
        email: { type: String },
        res_id: { type: Number },
        res_name: { type: String },
        reviews: [],
        connected_with_zomato: { type: Boolean },
        status: { type: Boolean, default: true }

    }
)

module.exports = {
    zomatoModel: mongoose.model('zomato', zomatoSchema)
}