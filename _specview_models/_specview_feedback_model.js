const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const feedbackSchema = new Schema(
    {
        client_id: String,
        customer_id: String,
	campaignId :String,
        customerData : { type : Object , default : null},
        replied : {type : Boolean,default : false},
        read : {type : Boolean , default : false}, 
        type:String,        
        service: String,
        price: String,
        staff: String,
        recommendProduct: String,
        visitAgain: String,
        message: String,
        createdOn : {type : Date, default : Date.now},
	feedbacktype : {type : String}
    }
)

module.exports = {
    feedbackModel: mongoose.model('feedback', feedbackSchema)
}
