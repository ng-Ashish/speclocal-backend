const request = require('request');
const facebookmodel = require('../../_specview_models/_specview_facebook_model').facebookModel;
const email = "hotelratnalok01@gmail.com";
function cron_hotel_ratnalok() {
    console.log('-------------------------------------' + email + ' cron start');
    facebookmodel.findOne({ "email": email }, function (err, result) {
        const pageId = result.linked_page.id;
        const accessToken = result.linked_page.access_token;
        getReviews(pageId, accessToken, null, result);
    })
}


function getReviews(pageId, accessToken, nextPageUrl, existingData) {
    firstHit(pageId, accessToken, nextPageUrl, existingData).then(promiseResponse => {
        // console.log('key :', promiseResponse);
        secondHit(promiseResponse, accessToken, existingData);
    })
}
function firstHit(pageId, accessToken, nextPageUrl, existingData) {
    return new Promise(function (resolve, reject) {
        const URL = `https://graph.facebook.com/v3.2/${pageId}/ratings?fields=has_review,has_rating,created_time,recommendation_type,open_graph_story,rating,review_text,reviewer,paging&access_token=${accessToken}&limit=10`;
        request({
            url: URL,
            method: 'get',
            headers: {
                "content-type": "application/json"
            }
        }, (error, resp, body) => {
            if (body == undefined) {
                console.log('plase check internet connection')
                return;
            } else {
                console.log('first hit ----------------------------->' + body)
                if (JSON.parse(body).paging === undefined) {
                    console.log('all data fetched >', email);
                    return;
                } else {
                    var result = JSON.parse(body).data.filter(function (o1) {
                        return !existingData.reviews.some(function (o2) {
                            return o1.open_graph_story.id == o2.open_graph_story.id;          // assumes unique id
                        });
                    }).map(function (o) {
                        return Object.assign({}, o)
                    });
                    // console.log(result);
                    result.forEach(element => {
                        facebookmodel.updateOne(
                            { "email": email },
                            {
                                $push: {
                                    "reviews": element
                                }
                            }, function (err, result) {
                                console.log('pushed record :', result, '<><><>', email);
                            })
                    })
                    nextPageUrl = JSON.parse(body).paging.next;
                    resolve(nextPageUrl);
                }
            }
        });
    })
}
function secondHit(nextPageUrl, accessToken, existingData) {
    var nextUrl = nextPageUrl + `&access_token=${accessToken}`
    request({
        url: nextUrl,
        method: 'get',
        headers: {
            "content-type": "application/json"
        }
    }, (error, resp, body) => {
        console.log('second hit -------------------------------->' + email);
        if (JSON.parse(body).data.length > 0) {
            if (JSON.parse(body).paging === undefined) {
                console.log('data fetched completed ' + email)
                return;
            } else {
                var result = JSON.parse(body).data.filter(function (o1) {
                    return !existingData.reviews.some(function (o2) {
                        return o1.open_graph_story.id == o2.open_graph_story.id;          // assumes unique id
                    });
                }).map(function (o) {
                    return Object.assign({}, o)
                });
                result.forEach(element => {
                    facebookmodel.update({
                        email: email,
                        $push: { "reviews": element }
                    }, function (err, result) {
                        console.log('pushed record :', result, '<><><>', email);
                    })
                })

                var key = JSON.parse(body).paging.next;
                secondHit(key, accessToken);
            }
        } else {
            console.log('data fetched completed ...',email);
            return;
        }
    });
}

module.exports = {
    cron_hotel_ratnalok: cron_hotel_ratnalok
}