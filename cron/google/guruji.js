const request = require('request');
const googlemodel = require('../../_specview_models/_specview_google_model').googleModel;
const email = "gurujipune81@gmail.com";
const config = require('../../_specview_config/_specview_dbconfig');
function cron_guruji_pune() {
    console.log('------------------google-------------------' + email + ' cron start');
    var access_token = '';
    googlemodel.findOne({ "email": email }, function (err, result) {
        var refreshToken = result.refresh_token;
        var URL_FOR_ACCESS_TOKEN = 'https://www.googleapis.com/oauth2/v4/token';
        let options = {
            url: URL_FOR_ACCESS_TOKEN,
            form: {
                refresh_token: refreshToken,
                client_id: config.CLIENT_ID,
                client_secret: config.CLIENT_SECRET,
                grant_type: "refresh_token"
            }
        };
        request.post(options, (error, resp, body) => {
            access_token = JSON.parse(body).access_token;
            getReviews(result.locations[0].name, access_token, result)
        });
    })
}
function getReviews(location, access_token, result) {
    firstHit(location, access_token, result).then(nextPageToken => {
        secondHit(location, access_token, result, nextPageToken);
    })
}
function firstHit(location, access_token, dataInDB) {
    return new Promise(function (resolve, reject) {
        const URL = 'https://mybusiness.googleapis.com/v4/' + location + '/reviews?access_token=' + access_token + '&pageSize=200'
        request.get({
            url: URL,
            method: 'get',
            headers: {
                "content-type": "application/json"
            }
        }, (error, resp, body) => {
            if (JSON.parse(body).nextPageToken == undefined) {
                console.log('cron completed')
            } else {
                console.log('first hit ---------------------------google-------------------------->');
                var result = JSON.parse(body).reviews.filter(function (o1) {
                    return !dataInDB.reviews.some(function (o2) {
                        return o1.reviewId == o2.reviewId;          // assumes unique id
                    });
                }).map(function (o) {
                    return Object.assign({}, o)
                });
                // console.log('new record result >>>',result);
                result.forEach(element => {
                    googlemodel.updateOne(
                        { "email": email },
                        {
                            $push: {
                                "reviews": element
                            }
                        }, function (err, result) {
                            console.log('pushed record :', result, '<><><>', email);
                        })
                })
                var nextPageToken = JSON.parse(body).nextPageToken;
                resolve(nextPageToken)
            }
        })
    })
}
function secondHit(location, access_token, dataInDB, nextPageToken) {
    const URL = 'https://mybusiness.googleapis.com/v4/' + location + '/reviews?access_token=' + access_token + '&pageSize=200&pageToken=' + nextPageToken;
    request({
        url: URL,
        method: 'get',
        headers: {
            "content-type": "application/json"
        }
    }, (error, resp, body) => {
        if (JSON.parse(body).nextPageToken == undefined) {
            console.log('cron job copmeted for ' + email);
        } else {

            var nextPageToken = JSON.parse(body).nextPageToken;
            console.log('second hit ----------------------google-------------------------->' + email);
            // console.log('body >>', body);
            var result = JSON.parse(body).reviews.filter(function (o1) {
                return !dataInDB.reviews.some(function (o2) {
                    return o1.reviewId == o2.reviewId;          // assumes unique id
                });
            }).map(function (o) {
                return Object.assign({}, o)
            });
            console.log('new record result >>>', result);
            if (result.length > 0) {
                result.forEach(element => {
                    googlemodel.updateOne(
                        { "email": email },
                        {
                            $push: {
                                "reviews": element
                            }
                        }, function (err, result) {
                            console.log('pushed record :', result, '<><><>', email);
                        })
                })
                // console.log('second hit ------------------------------------------------>' + email)
                secondHit(location, access_token, dataInDB, nextPageToken)
            } else {
                console.log('no new review')
                return;
            }
        }
    });
}
module.exports = {
    cron_guruji_pune_g: cron_guruji_pune
}