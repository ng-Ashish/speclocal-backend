const express = require('express');
const router = express.Router();
const issueController = require('../_specview_controllers/_specview_issue_controller')

router.post('/post',issueController.post);
router.get('/get',issueController.getAll);
router.get('/get/:char',issueController.get)
router.post('/sendmail',issueController.sendmail)
module.exports= router;