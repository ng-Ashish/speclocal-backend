const express = require('express')
const router = express.Router()
const campaignController = require('../_specview_controllers/_specview_campaign_controller')

router.post('/post', campaignController.post)
router.get('/get/:email', campaignController.get)
router.post('/campaignByMessage',campaignController.campaignByMessage)

module.exports = router