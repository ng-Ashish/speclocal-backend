const express = require('express')
const router = express.Router()
const inviteController = require('../_specview_controllers/_specview_invitation_controller')

router.post('/post', inviteController.post);
router.post('/sendmail',inviteController.sendmail)
router.get('/get',inviteController.getGoals);
router.get('/get/:email',inviteController.getGoal)
router.get('/count/:email',inviteController.count)
router.post('/invitationByMessage',inviteController.invitationByMessage);
router.get('/getGoalsAndInvitationsAdmin/:adminemail',inviteController.getGoalsAndInvitationsAdmin);
router.get('/getdaysleft/:email',inviteController.getdaysleft);
module.exports = router;