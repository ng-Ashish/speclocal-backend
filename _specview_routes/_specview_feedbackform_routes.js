var express = require('express')
var router = express.Router()
const feedbackformController = require('../_specview_controllers/_specview_feedbackform_controller')

router.get('/getfeedbackform' ,feedbackformController.getFeedbackForm)
router.post('/addfeedbackform', feedbackformController.addFeedbackForm)
router.delete('/deleteonefeedbackform/:id' , feedbackformController.deleteOneFeedbackForm)
router.put('/updatefeedbackform', feedbackformController.updateFeedbackForm)
router.get('/getonefeedbackform/:email' , feedbackformController.getOneFeedbackForm)
router.get('/getfeedbackformbyclientId/:clientId',feedbackformController.getFeedbackFormByClientId)

module.exports = router;