const express = require('express')
const router = express.Router()
const smsServiceCntrl = require('../_specview_controllers/_specview_smsService_controller')

router.post('/smsrequest',smsServiceCntrl.post)
router.get('/getsmsservice/:email',smsServiceCntrl.getsmsService)
router.get('/getallsmsService',smsServiceCntrl.getallsmsService)
router.put('/updatesmsservice',smsServiceCntrl.updateSmsService)

module.exports=router