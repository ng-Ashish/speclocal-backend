const express = require('express')
const router = express.Router()
const insightCntrl = require('../_specview_controllers/_specview_insights_controller')

router.post('/postinsightdata',insightCntrl.postInsightData)
router.get('/getoneinsightdata/:email',insightCntrl.getOneInsightData)
router.get('/getinsightdata',insightCntrl.getInsightData)

module.exports=router