var express =require('express');
var router = express.Router();
var platformcontroller =require('../_specview_controllers/_specview_platform_controller');

router.post('/addplatform',platformcontroller.addPlatform);
router.get('/getplatform' , platformcontroller.getPlatform);
router.delete('/deleteplatform/:platformName' , platformcontroller.deletePlatform);
router.put('/updateplatform' , platformcontroller.updatePlatform)
router.get('/getplatformbyplatformname/:platformName',platformcontroller.getPlatformByPlatformName)

module.exports = router;