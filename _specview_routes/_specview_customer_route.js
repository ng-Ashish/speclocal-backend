var express = require('express');
var router = express.Router();
var customercontroller = require('../_specview_controllers/_specview_customer_controller');

router.post('/post',customercontroller.post);
router.get('/get/:char',customercontroller.getCustomer);
router.get('/getcampaignmsg/:email',customercontroller.getCampaginCustomer);
router.delete('/deletecustomer/:client_id/:customer_id',customercontroller.deleteCustomer);
router.post('/deletecustomers/:client_id',customercontroller.deleteCustomers)
router.put('/updatecustomer',customercontroller.updateCustomer);
router.post('/updatecampaigned/:client_id/:status',customercontroller.updateCampaign)
router.post('/updatefeedbacked/:client_id/:status',customercontroller.updateFeedbacked)

module.exports=router;