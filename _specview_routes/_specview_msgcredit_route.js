const express = require('express')
const router = express.Router()
const msgcreditCtrl = require('../_specview_controllers/_specview_msgcredit_controller')

router.post('/post',msgcreditCtrl.post),
router.get('/count',msgcreditCtrl.getCount)
router.get('/getalldata',msgcreditCtrl.getalldate)
router.put('/updatecredit',msgcreditCtrl.updateCredit)
module.exports=router