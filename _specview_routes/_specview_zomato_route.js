var express = require('express');
var router = express.Router();
var zomatoController = require('../_specview_controllers/_specview_zomato_controller');

router.post('/post', zomatoController.post);
router.get('/get/:char',zomatoController.getData)
router.get('/getreview/:char',zomatoController.getReview)
router.put('/addreview',zomatoController.addReview)
router.get('/filterreview/:email',zomatoController.filterRevirew)
module.exports = router;