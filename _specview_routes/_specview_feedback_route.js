const express = require('express')
const router= express.Router()
const feedbackController = require('../_specview_controllers/_specview_feedback_controller');


router.post('/post',feedbackController.post);
router.get('/get',feedbackController.get);
router.get('/check/:customer_id/:campaignId',feedbackController.getOne)
router.get('/get/:client_id',feedbackController.getfeedback)
router.post('/feedbackreplied',feedbackController.feedbackreplied);
router.get('/getfeebackurl/:client_id',feedbackController.feedBackUrl);
router.post('/getreviewbyyearmonth',feedbackController.getReviewbyyearmonth);
router.get('/getReviewsCountThisMonth/:email',feedbackController.getReviewsCountThisMonth);
router.post('/getlastsevendayreviews',feedbackController.getLastSevenDayReviews);
router.post('/getlast15dayreviews',feedbackController.getLast15DaysReviews);
router.get('/getanalyticsdatabyday/:email/:day',feedbackController.getAnalyticsDataByDay);

module.exports=router;