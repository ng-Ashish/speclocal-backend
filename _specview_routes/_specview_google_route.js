var express = require('express');
var router = express.Router();
var googleController = require('../_specview_controllers/_specview_google_controller');

router.post('/post', googleController.post);
router.get('/get/:char', googleController.getData);
router.get('/getreview/:char', googleController.getReview)
router.get('/getlastsevendaysreview/:email',googleController.getLastSevenDaysReviews)
router.get('/getlast15daysreview/:email',googleController.getLast15DaysReviews)

module.exports = router;