const express = require('express')
const router = express.Router()
const serviceTypeController = require('../_specview_controllers/_specview_servicetype_controller')

router.get('/getservicetype' , serviceTypeController.getServiceType ),
router.put('/updateservicetype' , serviceTypeController.updateServiceType)
router.post('/addservicetype' , serviceTypeController.addServiceType)
router.delete('/deleteservicetype/:businessServiceType' , serviceTypeController.deleteServicetype)

module.exports = router