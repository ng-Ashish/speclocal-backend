var router = require('express').Router();
var enquiryController = require('../_specview_controllers/_specview_enquiry_controller').enquiryController;

router.post('/addenquiry',enquiryController.addEnquiry);
router.get('/getallenquiry',enquiryController.getAllEnquiry);
router.post('/sendmail',enquiryController.sendmail);

module.exports = {
    router : router
}