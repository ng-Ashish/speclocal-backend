const express = require('express')
const router = express.Router();
const facebookcontroller = require('../_specview_controllers/_specview_facebook_controller');

router.post('/post', facebookcontroller.post);
router.get('/get/:char', facebookcontroller.getData)
router.get('/getreview/:char',facebookcontroller.getReview)
router.get('/getlastsevendayreview/:email',facebookcontroller.getLastSevenDayReviews)
router.get('/getlast15daysreviews/:email',facebookcontroller.getLast15DaysReviews)

module.exports=router