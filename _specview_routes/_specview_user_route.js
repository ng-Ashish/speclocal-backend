let jwtmiddleware = require('../_specview_jwt/_specview_jwt_controller');
var express = require('express');
var router = express.Router()
var usercontroller = require('../_specview_controllers/_specview_user_controller');

// router.use(jwtmiddleware.checkToken);

router.get('/users', usercontroller.getUsers);
router.get('/user/:char', usercontroller.getUser);
router.get('/getuserstatus/:email',usercontroller.getUserStatus);
router.get('/alldata/:email', usercontroller.allUserData);
router.put('/updateuser', usercontroller.updateUser);
router.put('/changestatus', usercontroller.changeStatus);
router.put('/changepassword', usercontroller.changePassword);
router.delete('/disconnectplatform/:email/:platform',usercontroller.disconnectPlatforms)
router.post('/addEmployee', usercontroller.addEmployee);
router.get('/getemployees/:adminemail',usercontroller.getEmployees);
router.put('/updateEmployee' , usercontroller.updateEmployee);
router.delete('/deleteemployee/:email',usercontroller.deleteEmployee);



module.exports = router;
