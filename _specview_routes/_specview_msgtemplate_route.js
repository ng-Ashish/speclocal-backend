const express = require('express')
const router = express.Router()
const templateCntrl = require('../_specview_controllers/_specview_msgtemplates_controller')

router.post('/addtemplatemessage',templateCntrl.addCampaignMessage)
router.get('/gettemplatemessage/:client_id',templateCntrl.getTemplateMessage)

module.exports=router;