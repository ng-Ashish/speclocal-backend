const express = require('express')
const router = express.Router()
const requestCntrl = require('../_specview_controllers/_specview_employeerequest_controller')

router.post('/request',requestCntrl.employeerequest)
router.put('/upadterequest',requestCntrl.updateRequest)
router.get('/allrequest',requestCntrl.getAllRequest)

module.exports=router;