const express = require('express');
const router = express.Router()
const authController = require('../_specview_controllers/_specview_auth_controller');

router.post('/register', authController.userSignUp)
router.post('/login', authController.userSignin)
router.post('/forgetpassword', authController.forgetpassword);

module.exports = router;