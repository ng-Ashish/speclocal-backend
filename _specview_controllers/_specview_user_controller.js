const userModel = require('../_specview_models/_specview_user_model').userModel
const facebookModel = require('../_specview_models/_specview_facebook_model').facebookModel
const googleModel = require('../_specview_models/_specview_google_model').googleModel
const zomatoModel = require('../_specview_models/_specview_zomato_model').zomatoModel
const inviteModel = require('../_specview_models/_specview_invitation_model').inviteModel
const nodemailer = require('nodemailer')
const smtpTransport = require('nodemailer-smtp-transport');
const crypto = require('crypto')

const getUsers = (req, res) => {
    userModel.find({role : 'user'},(err, users) => {
        if (err) {
            throw err;
            console.log(err)
        } else if (!users) {
            res.json({
                success: false, msg: 'notfound'
            })
        } else {
            res.json({
                success: true, data: users
            })
        }
    })
}

const getUser = (req, res) => {
    userModel.findOne({ email: req.params.char }, function (err, result) {
        if (err) {
            res.json({ success: false, msg: "Error" })
        }
        else if (!result || result == null) {
            res.json({ success: false, msg: "not found" })
        }
        else {

            const decryptpassword = result.decrypt(result.password)
            result.password = decryptpassword
            res.json({ success: true, msg: "found", data: result })
        }
    })
}

const changeStatus = (req, res) => {
    userModel.findOneAndUpdate({ email: req.body.email }, req.body, { new: true }, (err, status) => {
        if (err) {
            throw err;
            console.log(err)
        } else if (!status) {
            res.json({
                success: false, msg: 'tryagain'
            })
        } else {
            res.json({
                success: true, data: status
            })
        }
    })
}

const changePassword = (req, res) => {
   userModel.findOne({email:req.body.email},(err,user)=>{
       if(err){
           throw err
       }
       else if(!user || user==null){
           res.json({success:false , msg:"user not found"})
       }else{
        let userpassword = user.decrypt(user.password)
        const validPassword=(userpassword===req.body.password)
           if(validPassword){
              user.password= user.encrypt(req.body.newPassword)
               user.save((err)=>{
                    if(err){
                        throw err
                    }
                    else{
                        res.json({success:true ,msg:"password changed"})
                    }
               })
           }else{
               res.json({success:false , msg:"wrong password"})
           }
       }
   })
}

    const updateUser = (req, res) => {
        userModel.find({email:req.body.email},(err,user)=>{
        if(err){
            throw err
        }else if(!user){
            res.json({success:false,msg:"user not found"})
        }else{
           
    var cipher = crypto.createCipher('aes-256-cbc', 'd6F3Efeq')
    var crypted = cipher.update(req.body.password, 'utf8', 'hex')
    crypted += cipher.final('hex');
    req.body.password=crypted
            userModel.update({ email: req.body.email }, req.body, { new: true }, (err, udata) => {
                if (err) {
                    res.json({ success: false, msg: "Error" })
                }
                else if (!udata || udata == null) {
                    
                    res.json({ success: false, msg: "not found" })
                }
                else {
                    res.json({ success: true, msg: "updated", data: udata })
                }
            })
        }
        
    })
}

const addEmployee = (req,res)=>{
    var user = new userModel()
    // var employee = new userModel()
    console.log(req.body)
    user.firstname = req.body.firstname;
    user.lastname = req.body.lastname;
    user.email = req.body.email;
    user.role = req.body.role
    user.password = user.encrypt(req.body.password)
    user.contactno = req.body.contactno;
    user.adminemail = req.body.adminemail;
    user.businessname = req.body.businessname;
    user.address = req.body.address	

    user.save((err , result)=>{
        if(err){
            throw err
        }else{
	var invite = new inviteModel()
            invite.email = req.body.email;
            invite.adminemail = req.body.adminemail;
            invite.goal = 10;
            invite.invitation = 10;
            invite.quick_invite = 0;
            invite.save((err , res)=>{
                if(err){
                    throw err
                }else{
                    // res.json({success:true , msg:"employee Added"})
                }
            })
            
            res.json({success:true , msg:"employee Added"})
        }
    })

}

const getEmployees = (req , res)=>{
userModel.find({adminemail : req.params.adminemail},(err , employees)=>{
 if(err){
     throw err;
 }else if(!employees){
     res.json({success : false , msg : 'Not Found'})
 }else{
     res.json({success:true , data : employees});
 }
})
}

const updateEmployee = (req , res)=>{
    console.log(req.body)
    userModel.findOneAndUpdate({ email: req.body.email}, req.body, { new: true }, (err, udata) => {
        if (err) {
            res.json({ success: false, msg: "Error" })
        }
        else if (!udata || udata == null) {
            
            res.json({ success: false, msg: "not found" })
        }
        else {
            res.json({ success: true, msg: "updated", data: udata })
        }
    })
}

const deleteEmployee = (req,res)=>{
userModel.findOneAndDelete({email : req.params.email},(err ,emp)=>{
    if (err) {
        res.json({ success: false, msg: "Error" })
    }else if(!emp){
        res.json({ success: false, msg: "Not Found" })
    }else{
	inviteModel.findOneAndDelete({email : req.params.email},(err ,invite)=>{
		if(err){
		res.json({ success: false, msg: "Error" })
		}else if(!invite){
		res.json({ success: false, msg: "Not Found" })
		}else{
		 res.json({ success: true, msg: "deleted" })
		}
	})
    }
})
}


const disconnectPlatforms=(req,res)=>{
    userModel.find({email:req.params.email},(err,user)=>{
        if(err){
            throw err
        }else if(!user){
            res.json({success:false , msg:"user not found"})
        }else{
            if(req.params.platform=='facebook'){
                facebookModel.findOneAndRemove({ email: req.params.email }, {}, (err, result) => {
                    if (err) {
                        throw err
                    } else {
                        res.json({ success: true, data: result })
                    }
                })
            }else if(req.params.platform=='google'){
                googleModel.findOneAndRemove({ email: req.params.email }, {}, (err, result) => {
                    if (err) {
                        throw err
                    } else {
                        res.json({ success: true, data: result })
                    }
                })
            }else{
                zomatoModel.findOneAndRemove({ email: req.params.email }, {}, (err, result) => {
                    if (err) {
                        throw err
                    } else {
                        res.json({ success: true, data: result })
                    }
                })
            }
        }
    })
}

const getUserStatus = (req, res) => {
    userModel.findOne({ email: req.params.email }, function (err, user) {
        if (err) {
            res.json({ success: false, msg: "Error" })
        }
        else if (!user || user == null) {
            res.json({ success: false, msg: "not found" })
        }
        else {
            res.json({ success: true, status: user.status })
        }
    })
}


const allUserData = (req, res) => {

    facebookModel.findOne({ email: req.params.email }, (err, fbdata) => {
        if (err) {
            throw err
        }
        else if (fbdata == null) {
            res.json({ success: false, connected_with_facebook: false })
        } else {
            googleModel.findOne({ email: req.params.email }, (err, gdata) => {
                if (err) {
                    throw err
                }
                else if (gdata == null) {
                    res.json({ success: false, connected_with_google: false })
                } else {
                    zomatoModel.findOne({ email: req.params.email }, (err, zdata) => {
                        if (err) {
                            throw err
                        }
                        else if (zdata == null) {


                            res.json({ success: false, msg: 'invalid email', connected_with_zomato: false })
                        } else {

                            res.json({ success: true, facebook: fbdata, google: gdata, zomato: zdata })
                        }
                    })
                }
            })
        }
    })



}




module.exports = {
    getUsers,
    getUser,
    allUserData,
    updateUser,
    changeStatus,
    changePassword,
    disconnectPlatforms,
    addEmployee,
    getEmployees,
    updateEmployee,
    deleteEmployee ,
    getUserStatus 
}
