const zomatoModel = require('../_specview_models/_specview_zomato_model').zomatoModel;
const moment = require('moment')

exports.post = (req, res) => {
    var review = new zomatoModel();
    review.email = req.body.email;
    review.res_id = req.body.res_id;
    review.res_name = req.body.res_name;
    review.reviews = req.body.reviews;

    if (req.body.email == null || req.body.email == '' ||
        req.body.res_id == null || req.body.res_id == '' ||
        req.body.res_name == null || req.body.res_name == '' ||
        req.body.reviews == null || req.body.reviews == '') {
        res.json({ success: false, message: "fields" });
    }
    else {
        review.save(function (err) {
            if (err) {
                res.json({ success: false, message: "exists" });
            }
            else {
                res.json({ success: true })
            }
        })
    }
  
}



exports.getData = (req, res) => {
    zomatoModel.findOne({ email: req.params.char }, function (err, review) {
        if (err) {
            res.json({ success: false, msg: "Error" })
        }
        else if (!review || review == null) {
            res.json({ success: false, msg: "not found", connected_with_zomato: false })
        }
        else {
            res.json({ success: true, msg: "found", connected_with_zomato: true, data: review })
        }
    })
}

exports.getReview = (req, res) => {
    zomatoModel.find({ email: req.params.char }, 'reviews', function (err, onlyReview) {
        if (err) {
            res.json({ success: false, msg: "Error" })
        }
        else if (!onlyReview || onlyReview == null) {
            res.json({ success: false, msg: "not found" })
        }
        else {
            res.json({ success: true, msg: "found", data: onlyReview })
        }
    })
}

exports.addReview = (req, res) => {
    zomatoModel.find({ 'review.id': { $in: [req.body.review] } }, function (err, id) {
        if (err) {
            throw err
        }
        if (id == req.body.review) {
            res.json({ msg: "id match" })
        }
        else {
            zomatoModel.findOneAndUpdate({ email: req.body.email }, { $push: { reviews: { $each: [req.body.reviews] } } }, { new: true }, function (err, udata) {
                if (err) {
                    res.json({ success: false, msg: "Error" })
                } else {
                    res.json({ success: true, msg: "successfully added", data: udata })
                }
            })
        }
    })

}

exports.filterRevirew = (req, res) => {
    zomatoModel.findOne({ email: req.params.email },{'reviews.review.review_time_friendly':{$lte: 'Apr 22, 2017'}
    }, function (err, response) {
        if (!err) {
            console.log(response)
        }
    });
}