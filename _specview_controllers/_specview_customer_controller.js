const mongoose = require('mongoose')
const customerModel = require('../_specview_models/_specview_customer_model').customerModel;
mongoose.set('useFindAndModify', false);


const post = (req, res) => {
    var customer = new customerModel();

    customer.client_id=req.body.client_id;
    customer.email = req.body.email;
    customer.customers = req.body.customers;

    customerModel.countDocuments({ email: req.body.email }, function (err, data) {

        if (err) {
            throw err
        }

        else if (data) {

            console.log(req.body.customers)
            customerModel.findOneAndUpdate({ email: req.body.email }, { $push: { customers: req.body.customers } }, { new: true }, function (err, udata) {

                if (err) {
                    res.json({ success: false, msg: "Error" })
                }
                else {
                    res.json({ success: true, msg: "successfully added" })
                }
            })
        } else {

            customer.save(function (err) {
                if (err) {
                    console.log(err)
                    res.json({ success: false, msg: "error" })
                }
                else {

                    res.json({ success: true, msg: "posted" })
                }
            })
        }
    });
}



const getCustomer = (req, res) => {
    customerModel.findOne({ email: req.params.char }, 'customers', (err, allcustomer) => {
        if (err) {
            throw err
        }
        else if (!allcustomer || allcustomer == null) {
            res.json({ success: false, msg: "notfound" })
        }
        else {
		allcustomer.customers.forEach((cust,index)=>{
			allcustomer.customers[index].id = index + 1;
			allcustomer.customers[index].flag = false;
			
		});
		console.log(allcustomer)
		/*allcustomer = {
			customers : revisedcustomer  
		}*/
            res.json({ success: true, data: allcustomer })
        }
    })
}

const getCampaginCustomer = (req, res) => {
	
    customerModel.find({ email: req.params.email }, 'customers', (err, campmsg) => {
        if (err) {
            throw err
        } else 
	if (!campmsg || campmsg == null || campmsg.length == 0) {
 
            res.json({ success: false, msg: "campmsg not found" })
        } else {
		console.log(campmsg)
            let customers = campmsg[0].customers;
            var filtered = customers.filter(cust=>{
                return cust.campaignmsg == 0;
            })
            
            res.json({success:true , data:filtered})

        }
    })
}

const deleteCustomer = (req,res)=>{
    customerModel.findOneAndUpdate({client_id:req.params.client_id},{$pull:{customers:{_id:req.params.customer_id}}},(err,deleteDate)=>{
        if(err){
            throw err
        }else if(!deleteDate){
            res.json({success:false , msg:"could not delete"})
        }else{
            res.json({success:true , msg:"record Deleted"})
        }
    })
}

const deleteCustomers = (req,res)=>{
    req.body.customers.forEach(customer=>{
        customerModel.findOneAndUpdate({client_id:req.params.client_id},{$pull:{customers:{_id:customer._id}}},(err,deleteDate)=>{
            if(err){
                throw err
            }
        });
    });
    res.json({success:true , msg:"records Deleted"})
}

const updateCampaign =(req,res)=>{
    req.body.customers.forEach(customer_id=>{
        customerModel.findOneAndUpdate({client_id:req.params.client_id, "customers._id": customer_id},{$set:{'customers.$.campaignmsg':req.params.status}},(err,udata)=>{
            if(err){
                throw err
            }else if(!udata){
                res.json({success:false , msg:"not found"})
            }
        })
    })
    res.json({success:true , msg:"Record Updated"})
}

const updateFeedbacked =(req,res)=>{
    req.body.customers.forEach(customer_id=>{
        customerModel.findOneAndUpdate({client_id:req.params.client_id, "customers._id": customer_id},{$set:{'customers.$.is_feedback':req.params.status}},(err,udata)=>{
            if(err){
                throw err
            }else if(!udata){
                res.json({success:false , msg:"not found"})
            }
        })
    })
    res.json({success:true , msg:"Record Updated"})
}

const updateCustomer = (req,res)=>{

    customerModel.findOneAndUpdate({client_id:req.body.client_id, 'customers._id':req.body._id},
    {
        $set:{
            "customers.$.name":req.body.name,
            "customers.$.phoneno":req.body.phoneno,
            "customers.$.email":req.body.email,
            "customers.$.occupation":req.body.occupation,
            "customers.$.birthdate":req.body.birthdate,
            "customers.$.anniversary":req.body.anniversary,
            "customers.$.address":req.body.address,
            "customers.$.campaignmsg":req.body.campaignmsg,
            "customers.$.is_feedback":req.body.is_feedback
        }
    },(err,udata)=>{
        if(err){
            throw err
        }else if(!udata){
            res.json({success:false , msg:"not found"})
        }else{
            res.json({success:true , msg:"Record updated"})
        }
    })
}

module.exports = {
    post,
    getCustomer,
    getCampaginCustomer,
    deleteCustomer,
    deleteCustomers,
    updateCustomer,
    updateCampaign,
    updateFeedbacked
}	