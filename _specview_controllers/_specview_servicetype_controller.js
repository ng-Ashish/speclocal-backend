const serviceTypeModel = require('../_specview_models/_specview_servicetype_model').serviceTypeModel

const addServiceType = (req, res) => {
    serviceTypeDoc = new serviceTypeModel;

    serviceTypeDoc.businessServiceType = req.body.businessServiceType


    serviceTypeModel.findOne({businessServiceType : req.body.businessServiceType} ,(err, serviceType) => {
        if(err){
            throw err
        }else if(serviceType){
            console.log("service type : " ,serviceType )
            res.json({success : false , msg : 'service type already exits'})
        }else{
            serviceTypeDoc.save((err, serviceTypedata) =>{
                console.log("service saved : " ,serviceTypedata )
                if(err){
                    throw err
                }else {
                    res.json({success : true , msg : "service type added" , data: serviceTypedata})
                }
            })
        }
    })
   
}

const getServiceType = (req , res) => {
    serviceTypeModel.find((err,serviceType) => {
        if(err){
            throw err
        }else if(!serviceType){
            res.json({
                success : false , msg : "not found"
            })
            }else{
                res.json({
                    success : true , data : serviceType
                })
        }
    })
}

const updateServiceType = (req , res) => {
    console.log(req.body)
    serviceTypeModel.findOneAndUpdate({_id : req.body.id},req.body , {new : true}, (err,servicedata)=>{
        if(err){
            res.json({success : false , msg : "error"})
        }else if(!servicedata){
            res.json({success : false , msg : "not found"})
        }else{
            res.json({success : true , msg : "updated" , data:servicedata })
        }
    })
}



const deleteServicetype = (req , res) => {
   serviceTypeModel.findOneAndDelete({businessServiceType : req.params.businessServiceType} , (err ,serviceType) =>{
       if(err){
           throw err
       }else if(serviceType){
             res.json({success: true, msg: "deleted"})    
       }else{
             res.json({success: false , msg : "not found"})
       }
   })
}

module.exports = {
    addServiceType,
    getServiceType,
    deleteServicetype,
    updateServiceType,
}

