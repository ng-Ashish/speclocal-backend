const templateModel = require('../_specview_models/_specview_msgtemplates_model').msgtemplateModel
const userModel = require('../_specview_models/_specview_user_model').userModel

const addCampaignMessage =(req,res)=>{
    var message = new templateModel()
    message.client_id = req.body.client_id
    message.templatename =req.body.templatename
    message.messagebody = req.body.messagebody

    message.save((err)=>{
        if(err){
            throw err
        }else{
            res.json({success:true , msg:"posted"})
        }
    })
}

const getTemplateMessage =(req,res)=>{
console.log("params:",req.params)
    userModel.findOne({email:req.params.client_id},(err ,user)=>{
        console.log("user 1:" ,user)
        if(err){
            throw err
        }else if(!user){
            res.json({success:false ,msg:"user not found"})
        }else{
            console.log("user :",user._id)
        templateModel.find({client_id:user._id},(err,data)=>{
        if(err){
            throw err
        }else if(!data){
            res.json({success:false , msg:"not found"})
        }
        else{
            res.json({success:true , msg:"found",data:data})
        }
    })
        }
    })

}
module.exports={
    addCampaignMessage,
    getTemplateMessage
}