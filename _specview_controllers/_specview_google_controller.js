const googleModel = require('../_specview_models/_specview_google_model').googleModel;
const MongoClient = require('mongodb').MongoClient;
const dbName = 'specviews';
const url = 'mongodb://localhost:27017/specviews'


exports.post = (req, res) => {
    var google = new googleModel()

    google.email = req.body.email;
    google.accounts = req.body.accounts;
    google.locations = req.body.locations;
    google.linked_location = req.body.linked_location;
    google.total_review_count = req.body.total_review_count;
    google.average_rating = req.body.average_rating;
    google.refresh_token = req.body.refresh_token;
    google.linked_account = req.body.linked_account;
    google.reviews = req.body.reviews

    google.save(function (err) {
        if (err) {
            res.json({ success: false, msg: "Error" })
        }
        else {
            res.json({ success: true, msg: "Posted" })
        }
    })
}

exports.getData = (req, res) => {
    googleModel.findOne({ email: req.params.char }, function (err, data) {
        if (err) {
            res.json({ success: false, msg: "Error" })
        }
        else if (!data || data == null) {
            res.json({ success: false, msg: "not found", connected_with_google: false })
        }
        else {
            res.json({ success: true, msg: "found", connected_with_google: true, data: data })
        }
    })
}

exports.getReview = (req, res) => {
    googleModel.findOne({ email: req.params.char }, 'reviews', function (err, review) {
        if (err) {
            res.json({ success: false, msg: "error" })
        }
        else if (!review || review == null) {
            res.json({ success: false, msg: "not found" })
        }
        else {

//		var review  = review.reviews;
console.log('google ======================>',review.reviews.slice(0,200))
		var filter = review.reviews.slice(0,200);
		var sorted = filter.sort(function (a, b) {
		    var dateA = new Date(a.createTime), dateB = new Date(b.createTime);
		    return dateB - dateA ;
		});
		var newreview = {
			reviews : sorted
		}
            res.json({ success: true, msg: "found", data: newreview })
        }
    })
}


exports.getLastSevenDaysReviews = (req, res) => {
    
    MongoClient.connect(url, function (err, record) {
        const col = record.db(dbName).collection('googles');
        col.aggregate([
            {
                $match: { email: req.params.email }
            },
            { $unwind: "$reviews" },
            {
                $project: {
                    reviewsdate: {
                        $dateFromString: {
                            dateString: '$reviews.createTime'
                        }
                    },
                    reviews:"$reviews"

                }
            },
            {
                $match: {
                    reviewsdate: { 
                        '$lte':new Date(),
                        '$gte': new Date((new Date().getTime() - (7 * 24 * 60 * 60 * 1000))) 
                    }
                }
            },
            

        ]).toArray((err,result)=>{
            if(err){
                throw err
            }else if(!result){
                res.json({success:false , msg:"not found"})
            }else{
                res.json({success:true , msg:"found" , data:result})
            }
        })
    })
}


exports.getLast15DaysReviews = (req, res) => {
    MongoClient.connect(url, function (err, client) {
        const col = client.db(dbName).collection('googles')
        col.aggregate([
            {
                $match: { email: req.params.email }
            },
            { $unwind: '$reviews' },
            {
                $project: {
                    reviewsdate: {
                        $dateFromString: {
                            dateString: '$reviews.createTime'
                        }
                    },
                    reviews: '$reviews'
                }
            },
            {
                $match: {
                    reviewsdate: {
                        '$lte': new Date(),
                        '$gte': new Date((new Date().getTime() - (15 * 24 * 60 * 60 * 1000)))
                    }
                }
            }
        ]).toArray((err, result) => {
            if (err) {
                throw err
            } else if (!result) {
                res.json({ success: false, msg: "not found" })
            } else {
                res.json({ success: true, msg: "found", data: result })
            }
        })
    })
}
