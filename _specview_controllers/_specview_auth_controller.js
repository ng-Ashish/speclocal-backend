const userModel = require('../_specview_models/_specview_user_model').userModel;
const campaigntemplate = require('../_specview_models/_specview_msgtemplates_model').msgtemplateModel
var jwt = require('jsonwebtoken');
var config = require('../_specview_jwt/_specview_jwt_secretkey')
const nodemailer = require('nodemailer')



exports.userSignUp = (req, res) => {

    console.log("userSignUp:" , req.body);
    const user = new userModel();
    user.firstname = req.body.firstname;
    user.lastname = req.body.lastname;
    user.dob = req.body.dob;
    user.gender = req.body.gender;
    user.businessname = req.body.businessname;
    user.businessaddress = req.body.businessname;
    user.fbpageurl = req.body.fbpageurl ;
    user.googlepageurl = req.body.googlepageurl;
    user.busuinessservicetype = req.body.busuinessservicetype;
    user.contactno = req.body.contactno;
    user.email = req.body.email;
    user.address = req.body.address;        
    user.password = user.encrypt(req.body.password);
    user.platformname  = req.body.platformname;
    user.smsserviceid = req.body.smsserviceid;
    user.smscampaining = req.body.smscampaining;
    user.birthday = req.body.birthday;
    user.anniversary = req.body.anniversary;
    user.role = req.body.role;
    user.msgcredit= req.body.msgcredit;
    user.noOfEmployees = req.body.noOfEmployees;
    user.sender = req.body.sender;
    user.route = req.body.route;
    user.autorenewal = req.body.autorenewal;
    user.renewvalamt  = req.body.renewvalamt;
    userModel.findOne({'email' : req.body.email},(err,data)=>{
        if(err){
            console.log(err);
        }else if(data){
            res.json({success : false , msg : 'user already exists'})
        }else{
            user.save((err,userData) => {
                if(err){
                    console.log(err);
                }else {
                    // res.json({success : true , msg : 'user added' , data: userData})
                    var template=
                        [{
                            client_id:userData._id,
                            templatename:"Feedback",
                            messagebody: "Thanks for choosing  We would love to know about your experience with us today. Visit", 
                        },
                        {
                            client_id:userData._id,
                            templatename: "Invitation",
                            messagebody:  "Speclocal invites you for our invite-only event taking place on {{Date}} between {{time}} to {{time}}. Book your seat now and get exclusive discounts and offers. Hurry Limited Seats only. Terms and Cond. apply. Sign up now: {{Link}}. Invite your {{number}} friends for our event and we shall have something special waiting for you."

                        },
                        {
                            client_id:userData._id,
                            templatename:"Referral",
                            messagebody:  "Avail {{number}}% discount every time one of your friend signs-up with us. Refer your friends now {{Link}}. Terms and Cond. apply.", 
                        },
                        {
                            client_id:userData._id,
                            templatename:"Sale",
                            messagebody: "Speclocal invites you for an exclusive sale taking place on {{Date}}. Visit us at {{Address}} between {{Time}} to {{Time}}", 
                        }];

                    template.forEach(temp=>{
                        const templateObj = new campaigntemplate();
                        templateObj.client_id =temp.client_id
                        templateObj.templatename = temp.templatename
                        templateObj.messagebody = temp.messagebody 
                        templateObj.save((err,tempData)=>{
                            if(err){
                                throw err
                            }else{
                                console.log(tempData._id)
                            }
                        })
                    })
                }
            })
        }

    })

}



//User Login 
exports.userSignin = (req, res) => {    
     console.log('username and password :',req.body)
    userModel.findOne({ email: req.body.email }, function (err, user) {
        if (err) {
            res.json({success:false , msg:"Error"})
            console.log(err)
        }
        else if (!user) {
            res.json({ success: false, message: "email" });
        }
        else {
            console.log("password:" + req.body.password)
            let userpassword = user.decrypt(user.password)
            const validPassword=(userpassword===req.body.password)
            console.log("valid" +validPassword)
            if(!validPassword){
                
                res.json({ success: false, message: "password" });

            } else if (user.status) {
                jwt.sign({ email: req.body.email }, config.secret, { expiresIn: '12h' }, function (err, token) {
                    console.log(token);
                    res.json({ success: true, data: user, token: token })
                })
            } else {
                res.json({ success: false, message: "status" })
            }
        }
    })
}

exports.forgetpassword = (req, res) => {
    console.log("req.boi :",req.body)
    userModel.findOne({email:req.body.email},(err,user)=>{
        console.log("user :",user)
        {if (err) throw err}
        if(!user){
            res.json({success:false , msg:"user not found"})
        }
        else{
            console.log('email:' + user.email, 'password:' + user.password )
            var userpassword = user.decrypt(user.password)
        var transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port:  587,
        secure: false, 
        auth: {
            user: 'sanketdeotarse09@gmail.com',
            pass: 'sanketoo7'
        },
            // use SSL    
     tls: {
         rejectUnauthorized: false
     }
     
});
    var mailOptions = {
        from:'sanketdeotarse09@gmail.com' ,
        to:req.body.email,
        subject:  "Speclocal - reset pasword",       
        
        html: "Your Email Is : " + user.email + "<br>"+
                "\n Your Password Is : " + userpassword                   
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
		console.log(error.message);
            return res.json({ status: 'true', msg: error.message });
        } else {
            console.log('Email sent: ' + info.response);
            return res.json({ success: true, msg: "Mail Send sucessfully" });
        }
    });
        }
    })
    
}