const feedbackFormModel = require('../_specview_models/_specview_feedbackform_model').feedbackFormModel


const addFeedbackForm = (req, res) => {
    feedbackForm = new feedbackFormModel()

    var formData = req.body;
    console.log(formData)
    feedbackForm.clientId=req.body.clientId
    feedbackForm.email = formData.email;
    feedbackForm.feedbackFormData = formData.feedbackFormData;
    feedbackForm.save((err, feedbackformData) => {
        if (err) {
            throw err
        } else {
            res.json({ success: true, msg: "feedbackform added", data: feedbackformData })
        }
    })

}

const getFeedbackForm = (req, res) => {
    feedbackFormModel.find((err, feedbackformData) => {
        if (err) {
            throw err
        } else if (!feedbackformData) {
            res.json({ success: false, msg: "not found" })
        } else {
            res.json({ success: true, data: feedbackformData })
        }
    })
}

const getOneFeedbackForm = (req, res) => {
    console.log("email" ,req.params.email)
    feedbackFormModel.findOne({email: req.params.email}, (err, feedbackForm) => {
        if(err){
            throw err
        } else if(feedbackForm){
            res.json({success : true,  data: feedbackForm})
        } else {
            res.json({ success: false , msg : "not found"})
        }
    }).sort({_id : -1})
}

const updateFeedbackForm = (req, res) => {
    feedbackFormModel.findOneAndUpdate({ email: req.body.email }, req.body, { new: true }, (err, feedbackFormData) => {
        if (err) {
            throw err
        } else if (!feedbackFormData) {
            res.json({ success: false, msg: "not found" })
        } else {
            res.json({ success: true, data: feedbackformData })
        }

    })
}

const deleteOneFeedbackForm = (req, res) => {
    feedbackFormModel.findOneAndDelete({ _id: req.params.id }, (err, feedbackForm) => {
        if (err) {
            throw err
        } else if (!feedbackForm) {
            res.json({ success: false, msg: "not found" })
        } else {
            res.json({ success: true, data: feedbackForm })
        }
    })
}


const getFeedbackFormByClientId=(req,res)=>{
    feedbackFormModel.findOne({clientId:req.params.clientId},(err,data)=>{
        if(err){
            throw err
        }else if(data==null){
            res.json({success:false , msg:'not found'})
        }else{
            res.json({success:true , msg:"found",data:data})
        }
    })
}
module.exports = {
    addFeedbackForm,
    getFeedbackForm,
    deleteOneFeedbackForm,
    updateFeedbackForm,
    getOneFeedbackForm,
    getFeedbackFormByClientId
}