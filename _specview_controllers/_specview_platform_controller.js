const platformModel = require('../_specview_models/_specview_platform_model').platformModel


const addPlatform = (req, res) => {
    console.log("add platform : ", req.body);
    platform = new platformModel

    platform.businessServiceType = req.body.businessServiceType,
        platform.platformName = req.body.platformName,
        platform.platformLogo = req.body.platformLogo,
        platform.platformLink = req.body.platformLink,

        console.log("platform : ", platform)
    platformModel.findOne({ platformName: req.body.platformName }, (err, platformname) => {
        if (err) {
            throw err
        } else if (platformname) {
            res.json({ success: false, msg: 'already exists' })
        } else {
            platform.save((err, platformData) => {
                if (err)
                    throw err
                else {
                    res.json({ success: true, msg: "platform added", data: platformData })
                }
            })
        }
    })

}

const getPlatform = (req, res) => {
    platformModel.find((err, platformData) => {
        if (err) {
            console.log(err)
        } else if (!platformData) {
            res.json({ success: false, msg: "not found" })
        } else {
            res.json({ success: true, data: platformData })
        }
    })
}

const getPlatformByPlatformName = (req, res) => {
    platformModel.find({ platformName: req.params.platformName }, (err, platform) => {
        if (err) {
            throw err
        } else if (platform == null) {
            res.json({ success: false, msg: "not found" })
        } else {
            res.json({ success: true, msg: "found", data: platform })
        }
    })
}

const updatePlatform = (req, res) => {
    platformModel.findOneAndUpdate({ _id: req.body.id }, req.body, { new: true }, (err, platformData) => {
        if (err) {
            res.json({ success: false, msg: "error" })
        } else if (!platformData) {
            res.json({ success: false, msg: "not found" })
        } else {
            res.json({ success: true, msg: "updated", data: platformData })
        }
    })
}


const deletePlatform = (req, res) => {
    platformModel.findOneAndDelete({ platformName: req.params.platformName }, (err, platformData) => {
        if (err) {
            throw err
        } else if (platformData) {
            res.json({ success: true, msg: "deleted" })

        } else {
            res.json({ success: false, msg: "not found" })
        }
    })
}

module.exports = {
    addPlatform,
    deletePlatform,
    updatePlatform,
    getPlatform,
    getPlatformByPlatformName
}