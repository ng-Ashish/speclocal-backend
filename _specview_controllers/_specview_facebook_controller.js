const facebookModel = require('../_specview_models/_specview_facebook_model').facebookModel;
const MongoClient = require('mongodb').MongoClient;
const dbName = 'specviews';
const url = 'mongodb://localhost:27017/specviews'


exports.post = (req, res) => {
    var facebook = new facebookModel();

    facebook.email = req.body.email;
    facebook.account = req.body.account;
    facebook.page_access_tokens = req.body.page_access_tokens;
    facebook.linked_page = req.body.linked_page;
    facebook.reviews = req.body.reviews;

    facebook.save(function (err) {
        if (err) {
            res.json({ success: false, msg: "Error" })
        }
        else {
            res.json({ success: true, msg: "posted" })
        }
    })
}

exports.getData = (req, res) => {
    facebookModel.findOne({ email: req.params.char }, function (err, data) {
        if (err) {
            res.json({ success: false, msg: "Error" })
        }
        else if (!data || data == null) {
            res.json({ success: false, msg: "not found", connected_with_facebook: false })
        }
        else {
            res.json({ success: true, msg: "found", data: data, connected_with_facebook: true })
        }
    })
}

exports.getReview = (req, res) => {
    facebookModel.find({ email: req.params.char }, 'reviews', function (err, review) {
        if (err) {
            res.json({ success: false, msg: "Error" })
        }
        else if (!review || review == null) {
            res.json({ success: false, msg: "not found" })
        }
        else {

	var filter = review[0].reviews.slice(0,200)
		var sorted = filter.sort(function (a, b) {
		    var dateA = new Date(a.created_time), dateB = new Date(b.created_time);
		    return dateB - dateA ;
		});
		var newreview = [{
			reviews : sorted
		}];
            res.json({ success: true, msg: "found", data: newreview })
        }
    })
}


exports.getLastSevenDayReviews = (req, res) => {
    
    MongoClient.connect(url,{ useNewUrlParser: true }, (err, record) => {
        const col = record.db(dbName).collection('facebooks');
        col.aggregate([
            {
                $match: { email: req.params.email },        

            },
            { $unwind: "$reviews" },
            {
                $project: {
                    reviewsdate: {
                        $dateFromString: {
                            dateString: '$reviews.created_time'
                        }
                    },
                    reviews: '$reviews'
                }
            },

            {
                $match: {
                    reviewsdate: {
                        '$lte': new Date(),
                        '$gte': new Date((new Date().getTime() - (7 * 24 * 60 * 60 * 1000)))
                    }
                }
            }
        ]).toArray((err,result)=>{
            if(err){
                throw err
            }else if(!result){
                res.json({success:false , msg:"not found"})
            }else{
                res.json({success:true , msg:"found",data:result})
            }
        })
    })
}


exports.getLast15DaysReviews = (req, res) => {
    MongoClient.connect(url, function (err, record) {
        const col = record.db(dbName).collection('facebooks')
        col.aggregate([
            {
                $match: { email: req.params.email }
            },
            { $unwind: '$reviews' },
            {
                $project: {
                    reviewsdate: {
                        $dateFromString: {
                            dateString: '$reviews.created_time'
                        }
                    },
                    reviews: '$reviews'
                }
            },
            {
                $match: {
                    reviewsdate: {
                        '$lte': new Date(),
                        '$gte': new Date((new Date().getTime() - (15 * 24 * 60 * 60 * 1000)))
                    }
                }
            }
        ]).toArray((err, result) => {
            if (err) {
                throw err
            } else if (!result) {
                res.json({ success: false, msg: "not found" })
            } else {
                res.json({ success: true, msg: "found", data: result })
            }
        })
    })
}
