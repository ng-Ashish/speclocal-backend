const inviteModel = require('../_specview_models/_specview_invitation_model').inviteModel
const config = require('../_specview_config/_specview_dbconfig');
const userModel = require('../_specview_models/_specview_user_model').userModel
const nodemailer = require('nodemailer')
const customerModel = require('../_specview_models/_specview_customer_model').customerModel;
const campaigntemplate = require('../_specview_models/_specview_msgtemplates_model').msgtemplateModel;
const msg91 = require('../msg91/msg91').msg91;

const post = (req, res) => {
    var invite = new inviteModel()
    invite.email = req.body.email;
    invite.adminemail = req.body.adminemail;
    invite.goal = req.body.goal;
    invite.invitation = req.body.invitation;
    invite.quick_invite = req.body.quick_invite;

    inviteModel.countDocuments({ email: req.body.email }, (err, data) => {
        if (err) {
            throw err
        } else if (data) {
            inviteModel.findOneAndUpdate({ email: req.body.email }, { $set: { goal: req.body.goal, invitation: req.body.invitation } }, { new: true }, (err, udata) => {
                if (err) {
                    throw err
                } else {
                    res.json({ success: true, msg: "updated" })
                }
            })
        } else {
            invite.save((err) => {
                if (err) {
                    throw err
                } else {
                    res.json({ success: true, msg: "post" })
                }
            })
        }
    })
}
const getGoals = (req, res) => {
    inviteModel.find((err, goals) => {
        if (err) {
            res.json({ success: false, msg: "error" })
        }
        else if (!goals) {
            res.json({ success: false, msg: "not found" })
        }
        else {
            res.json({ success: true, msg: "found", data: goals })
        }
    })
}

const getGoal = (req, res) => {
    inviteModel.findOne({ email: req.params.email }, (err, data) => {
        console.log("data goal and invitation: ", data)
        if (err) {
            res.json({ success: false, msg: "error" })
        }
        else if (!data) {
            res.json({ success: true, data: ({ "goal": 10, "invitation": 10 }) })
        } else {
            res.json({ success: true, msg: "found", data: data })
        }
    })
}
const getdaysleft = (req, res) => {
    var today = new Date();
    var oneDay = 24 * 60 * 60 * 1000;
    inviteModel.findOne({ email: req.params.email }, (err, data) => {
        //var updateddate = new Date();
        //updateddate = data.updatedate
        if (err) {
            res.json({ success: false, msg: "error" })
        }
        else if (!data) {
            res.json({ success: true, data: 30 })
        } else {
            var differentDays = Math.round(Math.abs((data.updatedate.getTime() - today.getTime()) / (oneDay)));
            var leftdays = 30 - differentDays
            res.json({ success: true, msg: "found", data: leftdays })
        }
    })
}
const count = (req, res) => {
    inviteModel.findOne({ email: req.params.email }, 'quick_invite_count', (err, inviteCount) => {
        if (err) { res.json({ success: false, msg: 'try agian' }) }
        if (!inviteCount) {
            console.log(inviteCount)
            res.json({ success: true, data: { "quick_invite_count": 0 } })
        }
        else {
            res.json({ success: true, data: inviteCount })
        }
    })
}


const sendmail = (req, res) => {
    var transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true,
        auth: {
            user: 'sanketdeotarse09@gmail.com',
            pass: 'sanketoo7'
        },

    });
    var mailOptions = {
        from: req.body.email,
        to: req.body.friendemail,
        subject: "Refer Friends",
        html: "Hello",
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error.message);
            return res.json({ status: 'true', msg: error.message });
        } else {
            console.log('Email sent: ' + info.response);
            return res.json({ success: true, msg: "Mail Send sucessfully" });
        }
    });
}

const getGoalsAndInvitationsAdmin = (req, res) => {
    var goalCount = 0;
    var invitationCount = 0;
    var quick_invite_count = 0;
    var cnt = 0;
    var empcount = 0;
    userModel.findOne({ email: req.params.adminemail, role: 'user' }, (err, user) => {
        if (err) {
            res.json({ success: false, msg: 'try agian' });
        } else if (user) {

            inviteModel.count({ adminemail: req.params.adminemail }, function (err, count) {
                empcount = count;
            })
            console.log("empCount", empcount);
            userModel.find({ adminemail: user.email, role: 'emp' }, (err, employees) => {
                employees.forEach(employee => {
                    console.log(employee.email);
                    inviteModel.findOne({ email: employee.email }, (err, emp) => {
                        if (err) {
                            res.json({ success: false, msg: 'err' });
                        } else if (emp) {
                            cnt = cnt + 1;
                            goalCount = goalCount + emp.goal;
                            invitationCount = invitationCount + emp.invitation;
                            quick_invite_count = quick_invite_count + emp.quick_invite_count;
                            if (cnt == empcount) {
                                res.json({ success: true, data: { invitation: invitationCount, goal: goalCount, quick_invite_count: quick_invite_count } })
                            }
                        }
                    })
                });
            })
        } else {
            res.json({ success: false, msg: 'user not found' });
        }
    })
}



const invitationByMessage = (req, res) => {
    var mobileNo = req.body.phoneno.split(',');
    console.log('request body for invitation : ', req.body);
    var greeting = ''
    if (req.body.greeting == undefined) {
        greeting = 'Hi '
    }
    userModel.findOne({ email: req.body.email }, (err, userData) => {
        if (err) {
            throw err
        } else if (userData == null) {
            res.json({ success: false, msg: "user not found" })
        } else {
            campaigntemplate.find({ client_id: userData._id }, (err, campaignData) => {
                if (err) {
                    throw err
                } else if (campaignData == null) {
                    res.json({ success: false, msg: "client not found" })
                } else {
                    var quickinviteMessage = campaignData.filter(temp => {
                        return temp.templatename == 'Feedback'
                    });
                    var referfriendMessage = campaignData.filter(temp => {
                        return temp.templatename == 'Referral'
                    });
                    customerModel.findOne({ email: req.body.email }, (err, result) => {
                        if (err) {
                            throw err
                        } else if (result != null) {

                            //ExistCustomer...............................
                            var existCustomer = result.customers.filter(cust => {
                                return cust.phoneno == req.body.phoneno
                            });
                            console.log("existCustomer :", existCustomer.length)
                            if (existCustomer.length > 0) {
                                var customer_id = existCustomer[0]._id;
                                var client_id = userData._id;
                                if (req.body.type == 'referfriend') {
                                    var message = greeting + ' ' + req.body.name + ' ' + referfriendMessage[0].messagebody;
                                    msg91.sendMessage(req.body.phoneno, config.msg91key, userData.route, userData.smsserviceid, req.body.dialCode, message);
                                    userModel.findOneAndUpdate({ email: req.body.email }, { $inc: { msgcredit: -1 } }, (err, udata) => {
                                        if (err) {
                                            throw err
                                        } else if (!udata) {
                                            console.log('data :', udata)
                                            //res.json({success:false ,msg:"not found"})
                                        } else {
                                            //console.log('udata success', udata);
                                            res.json({
                                                success: true,
                                                message: udata
                                            });
                                        }
                                    })
                                } else {
                                    //Send Link Quick Invite
                                    var message = greeting + ' ' + req.body.name + ' ' + quickinviteMessage[0].messagebody + ' ' + 'https://feedback.speclocal.com/feedback/servey/' + client_id + '/' + customer_id + '/xxxxxxxx';
                                    msg91.sendMessage(req.body.phoneno, config.msg91key, '4', userData.smsserviceid, req.body.dialCode, message);
                                    userModel.findOneAndUpdate({ email: req.body.email }, { $inc: { msgcredit: -1 } }, (err, udata) => {
                                        if (err) {
                                            throw err
                                        } else if (!udata) {
                                            //console.log('udata ', udata)
                                        } else {
                                            inviteModel.findOneAndUpdate(
                                                { email: req.body.email },
                                                { $inc: { quick_invite_count: 1 } }, (err, invitationData) => {
                                                    if (err) throw err
                                                    else {
                                                        res.json({
                                                            success: true,
                                                            message: udata
                                                        });
                                                    }
                                                })
                                        }
                                    })
                                }

                            }
                            else {
                                //Customer NOt Availabel
                                if (req.body.type == 'referfriend') {
                                    var message = greeting + ' ' + req.body.name + ' ' + referfriendMessage[0].messagebody;
                                    msg91.sendMessage(req.body.phoneno, config.msg91key, userData.route, userData.smsserviceid, req.body.dialCode, message);
                                    userModel.findOneAndUpdate({ email: req.body.email }, { $inc: { msgcredit: -1 } }, (err, udata) => {
                                        if (err) {
                                            throw err
                                        } else if (!udata) {
                                            console.log('data :', udata)
                                            //res.json({success:false ,msg:"not found"})
                                        } else {
                                            //console.log('udata success', udata);
                                            res.json({
                                                success: true,
                                                message: udata
                                            });
                                        }
                                    })
                                } else {
                                    var customer = {
                                        name: req.body.name,
                                        phoneno: req.body.phoneno,
                                        dialCode: req.body.dialCode,
                                        is_feedback: false,
                                        campaignmsg: true
                                    }
                                    customerModel.findOneAndUpdate({ email: req.body.email }, { $push: { customers: customer } }, { new: true }, function (err, udata) {
                                        if (err) {
                                            res.json({ success: false, msg: "Error" })
                                        } else {
                                            var newAddedCustomerId = udata.customers[udata.customers.length - 1]._id;
                                            var client_id = userData._id;

                                            //Send Link Quick Invite
                                            var message = greeting + ' ' + req.body.name + ' ' + quickinviteMessage[0].messagebody + ' ' + 'https://feedback.speclocal.com/feedback/servey/' + client_id + '/' + newAddedCustomerId + '/xxxxxxxx';
                                            msg91.sendMessage(req.body.phoneno, config.msg91key, '4', userData.smsserviceid, req.body.dialCode, message);
                                            userModel.findOneAndUpdate({ email: req.body.email }, { $inc: { msgcredit: -1 } }, (err, udata) => {
                                                if (err) {
                                                    throw err
                                                } else if (!udata) {
                                                    //console.log('udata ', udata)
                                                } else {
                                                    inviteModel.findOneAndUpdate(
                                                        { email: req.body.email },
                                                        { $inc: { quick_invite_count: 1 } }, (err, invitationData) => {
                                                            if (err) throw err
                                                            else {
                                                                res.json({
                                                                    success: true,
                                                                    message: udata
                                                                });
                                                            }
                                                        })
                                                }
                                            })
                                        }
                                    })
                                }
                            }
                        } else {
                            //client is not available .. so create new document in client and customer.
                            var customer = new customerModel()
                            customer.client_id = req.body.client_id;
                            customer.email = req.body.email;
                            customer.customers = req.body.customers
                            customer.save((err, result) => {
                                if (err) {
                                    throw err;
                                } else {
                                    if (req.body.type == 'referfriend') {
                                        var message = greeting + ' ' + req.body.name + ' ' + referfriendMessage[0].messagebody;
                                        msg91.sendMessage(req.body.phoneno, config.msg91key, userData.route, userData.smsserviceid, req.body.dialCode, message);
                                        userModel.findOneAndUpdate({ email: req.body.email }, { $inc: { msgcredit: -1 } }, (err, udata) => {
                                            if (err) {
                                                throw err
                                            } else if (!udata) {
                                                console.log('data :', udata)
                                                //res.json({success:false ,msg:"not found"})
                                            } else {
                                                //console.log('udata success', udata);
                                                res.json({
                                                    success: true,
                                                    message: udata
                                                });
                                            }
                                        })
                                    } else {
                                        var customer = {
                                            name: req.body.name,
                                            phoneno: req.body.phoneno,
                                            is_feedback: false,
                                            campaignmsg: true,
                                            dialCode: req.body.dialCode
                                        }
                                        customerModel.findOneAndUpdate({ email: req.body.email }, { $push: { customers: customer } }, { new: true }, function (err, udata) {
                                            if (err) {
                                                throw err
                                            } else {
                                                //console.log('new user objct', udata.customers[udata.customers.length - 1]._id)
                                                var newAddedCustomerId = udata.customers[udata.customers.length - 1]._id;
                                                //console.log('user details', udata.customers);
                                                var client_id = udata.client_id;

                                                //Send Link Quick Invite
                                                var message = greeting + ' ' + req.body.name + ' ' + quickinviteMessage[0].messagebody + ' ' + 'https://feedback.speclocal.com/feedback/servey/' + client_id + '/' + newAddedCustomerId + '/xxxxxxxx';
                                                msg91.sendMessage(req.body.phoneno, config.msg91key, '4', userData.smsserviceid, req.body.dialCode, message);
                                                userModel.findOneAndUpdate({ email: req.body.email }, { $inc: { msgcredit: -1 } }, (err, udata) => {
                                                    if (err) {
                                                        throw err
                                                    } else if (!udata) {
                                                        //console.log('udata ', udata)
                                                    } else {
                                                        inviteModel.findOneAndUpdate(
                                                            { email: req.body.email },
                                                            { $inc: { quick_invite_count: 1 } }, (err, invitationData) => {
                                                                if (err) throw err
                                                                else {
                                                                    res.json({
                                                                        success: true,
                                                                        message: udata
                                                                    });
                                                                }
                                                            })
                                                    }
                                                })

                                            }
                                        })
                                    }
                                }
                            })
                        }
                    })

                }
            })

        }
    })
}


module.exports = {
    post,
    getGoals,
    getGoal,
    count,
    invitationByMessage,
    sendmail,
    getGoalsAndInvitationsAdmin,
    getdaysleft
}
