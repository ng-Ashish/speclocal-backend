const smsService = require('../_specview_models/_specview_smsService_model').smsServiceModel
const userModel = require('../_specview_models/_specview_user_model').userModel

const post = (req, res) => {
    var service = new smsService();

    service.email = req.body.email;
    service.birthday = req.body.birthday ;
service.anniversary= req.body.anniversary;
service.smscampaign = req.body.smscampaign ;
service.lastname = req.body.lastname;
service.firstname= req.body.firstname;
service.contactno= req.body.contactno;

    smsService.countDocuments({email:req.body.email},(err,data)=>{
        if(err){
            throw err
        }
        else if(data){
            smsService.findOneAndUpdate(
                {email:req.body.email},
                {$set:{birthday:req.body.birthday,anniversary:req.body.anniversary,smscampaign:req.body.smscampaign}},(err,updata)=>{
                    if(err){
                        throw err
                    }else{
                        res.json({success:true  , msg:"updated"})
                    }
                })
        }else{
            service.save((err,result)=>{
                if(err){
                    throw err
                }else{
                    res.json({success:true , msg:"posted"})
                }
            })
        }
    })
}
const getsmsService = (req, res) => {
    smsService.find({ email: req.params.email }, (err, smsservice) => {
        if (err) {
            throw err
        } else if (!smsservice) {
            res.json({ success: false, msg: "not found" })
        } else {
            res.json({ success: true, msg: "found", data: smsservice })
        }
    })
}
const getallsmsService = (req, res) => {
    smsService.find({}, (err, smsservice) => {
        if (err) {
            throw err
        } else if (!smsservice) {
            res.json({ success: false, msg: "not found" })
        } else {
            res.json({ success: true, msg: "found", data: smsservice })
        }
    })
}
const updateSmsService = (req, res) => {
    userModel.findOneAndUpdate({ email: req.body.email }, { $set: { birthday: req.body.birthday, anniversary: req.body.anniversary,smscampaining:req.body.smscampaign } }, (err, udata) => {
        if (err) {
            throw err
        } else if (!udata) {
            res.json({ success: false, msg: "not found" })
        } else {
            smsService.updateMany({ email: req.body.email }, { $set: { status: true, approve_date: Date.now() } }, (err, status) => {
                if (err) {
                    throw err
                } else if (!status) {
                    res.json({ success: false, msg: "try again" })
                } else {
                    res.json({ success: true })
                }
            })
        }
    })
}



module.exports = {
    post,
    getsmsService,
    getallsmsService,
    updateSmsService
}