const feedbackModel = require('../_specview_models/_specview_feedback_model').feedbackModel;
const customerModel = require('../_specview_models/_specview_customer_model').customerModel;
const userModel = require('../_specview_models/_specview_user_model').userModel;
const config = require('../_specview_config/_specview_dbconfig');

const faceBookModel = require('../_specview_models/_specview_facebook_model').facebookModel;
const googleModel = require('../_specview_models/_specview_google_model').googleModel;
const MongoClient = require('mongodb').MongoClient;
const dbName = 'specviews';
const url = 'mongodb://localhost:27017/specviews';

const post = (req, res) => {
    console.log(req.body);
    userModel.findOne({ _id: req.body.client_id }, (err, client) => {
        if (err) {
            throw err
        }
        else if (!client) {
            res.json({ success: false, msg: "client not found" })
        }
        else if (client) {
            customerModel.findOne({ client_id: req.body.client_id }, (err, cust_client) => {
                if (err) {
                    res.json({ success: false, msg: "Error" })
                }
                else if (!cust_client) {
                    res.json({ success: false, msg: "cust_client not found" })
                }
                else {
                    // console.log('req.body.customer_id ----->', req.body.customer_id);
                    // console.log('cust_client ----->', cust_client);
                    cust_client.customers.forEach(customer => {
                        if (customer._id == req.body.customer_id) {
                            console.log('customer exist');
                            var feedback = new feedbackModel();
                            feedback.client_id = req.body.client_id
                            feedback.customer_id = req.body.customer_id;
                            feedback.campaignId = req.body.campaignId
                            feedback.type = req.body.type;
                            feedback.service = req.body.service;
                            feedback.price = req.body.price;
                            feedback.staff = req.body.staff;
                            feedback.recommendProduct = req.body.recommendProduct;
                            feedback.visitAgain = req.body.visitAgain;
                            feedback.message = req.body.message;
                            feedback.feedbacktype = req.body.feedbacktype;
                            feedback.save(function (err, result) {
                                if (err) {
                                    res.json({ success: false, msg: "Error" })
                                }
                                else {
                                    console.log('result >', result);
                                    customerModel.findOneAndUpdate({ client_id: req.body.client_id, "customers._id": req.body.customer_id }, { $set: { 'customers.$.is_feedback': true } }, (err, isfeedback) => {
                                        if (err) {
                                            res.json({ success: false, msg: "error" })
                                        }
                                        else if (!isfeedback) {
                                            res.json({ success: false, msg: "try again" })
                                        }
                                        else {
                                            console.log('isfeedback ', isfeedback)

                                            feedbackModel.updateOne({ _id: result._id }, { $set: { customerData: customer } }, { new: true }, (err, updatedRecord) => {
                                                console.log("resSend :", isfeedback)

                                                res.json({ success: true, data: isfeedback })
                                            })
                                        }
                                    })
                                }
                            });
                        } else {
                            // res.json({ success: false, msg: 'not valid customer ' })                                            
                        }
                    });
                }
            })
        }
    })
}

const get = (req, res) => {
    feedbackModel.find({}, null, { sort: { createdOn: -1 } }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: "Error" })
        }
        else if (!data) {
            res.json({ success: false, msg: "not found" })
        }
        else {
            res.json({ success: true, msg: "found", data: [data] })
        }
    })
}


const getOne = (req, res) => {
    var condition;
    if (req.params.campaignId === 'xxxxxxxx') {
        condition = { customer_id: req.params.customer_id }
    } else {
        condition = { customer_id: req.params.customer_id, campaignId: req.params.campaignId }
    }
    feedbackModel.find(condition, (err, data) => {
        if (err) {
            res.json({ success: false, msg: "Error" })
        }
        else if (data.length == 0) {
            res.json({ success: false })
        }
        else {
            // console.log('data: ', data)
            res.json({ success: true })
        }
    })
}


const getfeedback = (req, res) => {
    feedbackModel.find({ client_id: req.params.client_id }, (err, data) => {
        console.log("Data :", data)
        console.log("req params :", req.params)
        if (err) {
            res.json({ success: false, msg: "Error" })
        } else if (!data) {
            res.json({ success: false, msg: "not found" })
        } else {
            var feedbackdata = data.filter(res => {
                return res.client_id == req.params.client_id
            });
            feedbackdata = feedbackdata.filter(type => {
                return type.feedbacktype == 'negative'
            })
            console.log('feedbackdata  ', feedbackdata)
            // console.log('feedbcaktype ', feedbacktype)
            return res.send({
                success: true,
                data: feedbackdata
                //data:feedbacktype
            });
        }
    })
}
const feedbackreplied = (req, res) => {
    feedbackModel.findOne({ client_id: req.body.client_id, customer_id: req.body.customer_id }, (err, user) => {
        if (err) {
            throw err
        } else if (!user) {
            res.json({ success: false, msg: "not found" })
        } else {
            var msg91 = require("msg91")(config.msg91key, config.sender, config.route);
            var mobileNo = req.body.phoneno.split(',');
            var message = 'Hi ' + req.body.name + ', ' + req.body.message
            msg91.send(mobileNo, message, function (err, response) {

                feedbackModel.findOneAndUpdate({ client_id: req.body.client_id, customer_id: req.body.customer_id }, { $set: { replied: true } }, (err, udata) => {
                    if (err) {
                        throw err
                    } else if (!udata) {
                        console.log("udata : ", udata)
                    } else {
                        res.json({
                            success: true,
                            data: response
                        })
                    }
                })

            })
        }
    })
}

const feedBackUrl = (req, res) => {
    var clientId = req.params.client_id;
    console.log('clientI ', clientId);
    userModel.findOne({
        _id: req.params.client_id
    }, function (err, user) {
        if (err) {
            return res.json({
                success: false,
                msg: err
            });
        } else if (!user) {
            return res.json({
                success: false,
                msg: 'User is not in System'
            });
        } else {
            // console.log('user ',user);
            urlsObj = [];
            faceBookData(user.email).then(fbdata => {
                if (fbdata != null) {
                    urlsObj.push({ platform: 'fb', data: fbdata, show: true });
                } else {
                    urlsObj.push({ data: fbdata, show: false });
                }
                googleData(user.email).then(googledata => {
                    if (googledata != null) {
                        urlsObj.push({ platform: 'google', data: googledata, show: true });
                    } else {
                        urlsObj.push({ data: googledata, show: false });
                    }
                    res.send({
                        url: urlsObj
                    });
                })
            })
        }
    })
}

function faceBookData(email) {
    return new Promise(function (resolve, reject) {
        faceBookModel.findOne({
            email: email
        }, (err, fbdata) => {
            var fbUrl = [];
            if (fbdata == null) {
                resolve(null)
            } else {
                // fbUrl = fbdata.reviews.map(review => {
                //     return review.open_graph_story.data.seller.url
                // })
                // console.log('fbUrl  ', fbUrl);
                // urlOfSyncLocation = [...new Set(fbUrl)];
                // var fbObj = {
                //     url: urlOfSyncLocation[0],
                //     id: fbdata.page_access_tokens[0].id
                // }                
                resolve(fbdata.page_access_tokens[0].id)
            }
        });
    });
}
function googleData(email) {
    return new Promise(function (resolve, reject) {
        googleModel.findOne({
            email: email
        }, (err, googledata) => {
            urlOfSyncLocation = [];
            if (googledata == null) {
                resolve(null)
            } else {
                // googledata.reviews.forEach((review) => {
                //     googledata.locations.forEach((location) => {
                //         if (review.name.split('/')[3] === location.name.split('/')[3]) {
                //             urlOfSyncLocation.push(location.metadata.newReviewUrl);
                //         }
                //     })
                // });
                // urlOfSyncLocation = [...new Set(urlOfSyncLocation)];                
                resolve(googledata.locations[0].locationKey.placeId)
            }
        });
    });
}


function googleChartData(email, year) {
    // console.log('year >',year)
    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, function (err, client) {
            const col = client.db(dbName).collection('googles');
            col.aggregate([{
                $match: {
                    email: email,
                }
            }, {
                $unwind: "$reviews"
            }, {
                $group: {
                    _id: {
                        year: { $substr: ["$reviews.createTime", 0, 4] },
                        month: { $substr: ["$reviews.createTime", 5, 2] }
                    }, count: { $sum: 1 }
                }
            }]).toArray(function (err, result) {
                if (err) {
                    res.send({
                        data: err
                    });
                } else {
                    if (result == []) {
                        resolve(null);
                    } else {
                        var dataset = [];
                        result = result.filter(data => {
                            return parseInt(data._id.year) == year
                        })
                        // console.log('filtered data >', result)
                        for (let i = 0; i < result.length; i++) {
                            for (let j = 0; j < 12; j++) {
                                if (parseInt(result[i]._id.month) == j) {
                                    if (dataset[j] == 0) {
                                        dataset[j] = result[i].count;
                                    } else {
                                        dataset.push(result[i].count)
                                    }
                                } else {
                                    if (dataset[j] == 0) {
                                    } else {
                                        if (dataset[j] > 0) {

                                        } else {
                                            dataset.push(0);
                                        }
                                    }
                                }
                            }
                        }
                        resolve(dataset);
                        client.close();
                    }
                }
            });
        });
    })
}
function faceChartData(email, year) {
    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, function (err, client) {
            const col = client.db(dbName).collection('facebooks');
            col.aggregate([{
                $match: {
                    email: email
                }
            }, {
                $unwind: "$reviews"
            }, {
                $group: {
                    _id: {
                        year: { $substr: ["$reviews.created_time", 0, 4] },
                        month: { $substr: ["$reviews.created_time", 5, 2] }
                    }, count: { $sum: 1 }
                }
            }]).toArray(function (err, result) {
                var dataset = [];
                result = result.filter(data => {
                    return parseInt(data._id.year) == year
                    // console.log('fb >', parseInt(data._id.year), '==', year)
                })
                // console.log('filtered data >', result)
                if (err) {
                    res.send({
                        data: err
                    });
                } else {
                    dataset = [];
                    for (let i = 0; i < result.length; i++) {
                        for (let j = 0; j < 12; j++) {
                            if (parseInt(result[i]._id.month) == j) {
                                if (dataset[j] == 0) {
                                    dataset[j] = result[i].count;
                                } else {
                                    dataset.push(result[i].count)
                                }
                            } else {
                                if (dataset[j] == 0) {
                                } else {
                                    if (dataset[j] > 0) {

                                    } else {
                                        dataset.push(0);
                                    }
                                }
                            }
                        }
                    }
                    resolve(dataset);
                    client.close();
                }
            });
        });
    })
}
const getReviewbyyearmonth = (req, res) => {
    googleChartData(req.body.email, req.body.year).then(googleData => {
        var chartResponse = [];
        if (googleData == null) {
            chartResponse.push({
                platform: 'Google',
                data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                show: false
            });
        } else {
            chartResponse.push({
                platform: 'Google',
                data: googleData,
                show: true
            });
        }
        faceChartData(req.body.email, req.body.year).then(fbData => {
            if (fbData == null) {
                chartResponse.push({
                    platform: 'Facebook',
                    data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    show: false
                });
            } else {
                chartResponse.push({
                    platform: 'Facebook',
                    data: fbData,
                    show: true
                });
                res.send({
                    data: chartResponse
                });
            }
        })
    })
}

const getReviewsCountThisMonth = (req, res) => {
    fbCnt = 0;
    googleCnt = 0;
    totalCnt = 0;
    var month = new Date().getMonth();
    var year = new Date().getFullYear();

    faceBookModel.find({ email: req.params.email }, "reviews", (err, fbdata) => {
        if (err) {
            throw err
        } else if (fbdata) {
            // Facebook document exists
            console.log("fbdata : ", fbdata)
            if (fbdata[0].reviews) {
                // Facebook reviews available
                fbdata[0].reviews.forEach(review => {
                    if (new Date(review.created_time).getFullYear() == year && new Date(review.created_time).getMonth() == month) {
                        fbCnt++;
                    }
                });
                console.log('Facebook Count:', fbCnt);
                // Check Google Document
                googleModel.find({ email: req.params.email }, "reviews", (err, googledata) => {
                    if (err) {
                        throw err
                    } else if (googledata) {
                        // Google document exists
                        console.log(googledata);
                        if (googledata[0].reviews) {
                            googledata[0].reviews.forEach(review => {

                                if (new Date(review.createTime).getFullYear() == year && new Date(review.createTime).getMonth() == month) {
                                    googleCnt++;
                                }
                            });
                        }

                        console.log('Google Count:', googleCnt);
                        totalCnt = fbCnt + googleCnt;
                        console.log(totalCnt);
                        res.json({ success: true, reviewsCount: totalCnt })
                    } else {

                        // if google document not exists
                        totalCnt = fbCnt + googleCnt;
                        console.log(totalCnt);
                        res.json({ success: true, reviewsCount: totalCnt })
                    }
                });
            }
            else {
                // if facebook review not available ...then check google

                googleModel.find({ email: req.params.email }, "reviews", (err, googledata) => {
                    if (err) {
                        throw err
                    } else if (googledata) {
                        console.log(googledata);
                        if (googledata[0].reviews) {
                            googledata[0].reviews.forEach(review => {
                                // console.log(new Date(review.createTime).getFullYear());
                                // console.log(new Date(review.createTime).getMonth());
                                if (new Date(review.createTime).getFullYear() == year && new Date(review.createTime).getMonth() == month) {
                                    googleCnt++;
                                }
                            });
                        }


                        console.log('Google Count:', googleCnt);
                        totalCnt = fbCnt + googleCnt;
                        console.log(totalCnt);
                        res.json({ success: true, reviewsCount: totalCnt })
                    }
                });
            }

        } else {
            // if facebook doucment is not available...........then check google document............
            googleModel.find({ email: req.params.email }, "reviews", (err, googledata) => {
                if (err) {
                    throw err
                } else if (googledata) {
                    console.log(googledata);
                    if (googledata[0].reviews) {
                        googledata[0].reviews.forEach(review => {
                            // console.log(new Date(review.createTime).getFullYear());
                            // console.log(new Date(review.createTime).getMonth());
                            if (new Date(review.createTime).getFullYear() == year && new Date(review.createTime).getMonth() == month) {
                                googleCnt++;
                            }
                        });
                    }


                    console.log('Google Count:', googleCnt);
                    totalCnt = fbCnt + googleCnt;
                    console.log(totalCnt);
                    res.json({ success: true, reviewsCount: totalCnt })
                }
                else {
                    // if google document not exists

                    totalCnt = fbCnt + googleCnt;
                    console.log(totalCnt);
                    res.json({ success: true, reviewsCount: totalCnt })
                }
            });
        }
    })
}




//.............Last Seven Days review....................

function facebookgetLastSevenDayReviews(email, day) {
    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, (err, client) => {
            const col = client.db(dbName).collection('facebooks');
            col.aggregate([
                {
                    $match: { email: email },
                },
                { $unwind: "$reviews" },
                {
                    $project: {
                        reviews: {
                            $dateFromString: {
                                dateString: '$reviews.created_time'
                            }
                        },

                    }
                },
                {
                    $match: {
                        reviews: { '$gte': new Date((new Date().getTime() - (7 * 24 * 60 * 60 * 1000))) }
                    }
                },

                {
                    $group: {
                        _id: {
                            day: { $dayOfMonth: "$reviews" }
                        }, count: { $sum: 1 }

                    }
                }
            ]).toArray((err, result) => {
                var dataset = [];
                var today = new Date();
                var daysSorted = [];
                for (var i = 0; i < 7; i++) {
                    var newDate = new Date(today.setDate(today.getDate() - 1));
                    daysSorted.push(newDate);
                }
                for (let i = 0; i < result.length; i++) {
                    for (let j = 0; j < daysSorted.length; j++) {
                        if (result[i]._id.day == daysSorted[j].getDate()) {
                            if (dataset[j] == 0) {
                                dataset[j] = result[i].count
                            } else {
                                dataset.push(result[i].count)
                            }
                        } else {
                            if (dataset[j] == 0) {
                            } else {
                                if (dataset[j] > 0) {

                                } else {
                                    dataset.push(0);
                                }
                            }
                        }
                    }
                }
                console.log('7 facebook dataset2 >', dataset)

                resolve(dataset);
                client.close()
            })
        })
    })

}

function googlegetLastSevenDayReviews(email, day) {
    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, (err, client) => {
            const col = client.db(dbName).collection('googles');
            col.aggregate([
                {
                    $match: { email: email }
                },
                { $unwind: "$reviews" },
                {
                    $project: {
                        reviews: {
                            $dateFromString: {
                                dateString: '$reviews.createTime'
                            }
                        },

                    }
                },
                {
                    $match: {
                        reviews: { '$gte': new Date((new Date().getTime() - (7 * 24 * 60 * 60 * 1000))) }
                    }
                },
                {
                    $group: {
                        _id: {
                            day: { $dayOfMonth: "$reviews" }
                        }, count: { $sum: 1 }
                    }
                }

            ])
                .toArray((err, result) => {
                    var dataset = [];
                    var today = new Date();
                    var daysSorted = [];
                    for (var i = 0; i < 7; i++) {
                        var newDate = new Date(today.setDate(today.getDate() - 1));
                        daysSorted.push(newDate);
                    }

                    for (let i = 0; i < result.length; i++) {
                        for (let j = 0; j < daysSorted.length; j++) {
                            if (result[i]._id.day == daysSorted[j].getDate()) {
                                if (dataset[j] == 0) {
                                    dataset[j] = result[i].count
                                } else {
                                    dataset.push(result[i].count)
                                }
                            } else {
                                if (dataset[j] == 0) {
                                } else {
                                    if (dataset[j] > 0) {

                                    } else {
                                        dataset.push(0);
                                    }
                                }
                            }
                        }
                    }
                    console.log('7 google dataset2 >', dataset)

                    resolve(dataset);
                    client.close()
                })
        })
    })

}

const getLastSevenDayReviews = (req, res) => {
    var chartResponse = [];
    facebookgetLastSevenDayReviews(req.body.email, req.body.day).then(faceBookData => {
        console.log('facebook >>>', faceBookData)
        if (faceBookData == null) {
            chartResponse.push({
                platform: 'Facebook',
                data: [0, 0, 0, 0, 0, 0, 0],
                show: false
            })
        } else {
            chartResponse.push({
                platform: 'Facebook',
                data: faceBookData,
                show: true
            })
        }
        googlegetLastSevenDayReviews(req.body.email, req.body.day).then(googledata => {
            console.log('google >>>', googledata)
            if (googledata == null) {
                chartResponse.push({
                    platform: 'Google',
                    data: [0, 0, 0, 0, 0, 0, 0],
                    show: false
                });
            } else {
                chartResponse.push({
                    platform: 'Google',
                    data: googledata,
                    show: true
                });
                res.send({
                    data: chartResponse
                });
            }
        })
    })
}

//...............last seven Days review End...................


//...............last 15 days Reviews..........................
function facebookGetLast15DaysReviews(email, day) {
    console.log('hit for 15 day');
    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, (err, client) => {
            const col = client.db(dbName).collection('facebooks');
            col.aggregate([
                {
                    $match: { email: email },

                },
                { $unwind: "$reviews" },
                {
                    $project: {
                        reviews: {
                            $dateFromString: {
                                dateString: '$reviews.created_time'
                            }
                        },

                    }
                },
                {
                    $match: {
                        reviews: { '$gte': new Date((new Date().getTime() - (15 * 24 * 60 * 60 * 1000))) }
                    }
                },

                {
                    $group: {
                        _id: {
                            day: { $dayOfMonth: "$reviews" }
                        }, count: { $sum: 1 }

                    }
                }
            ]).toArray((err, result) => {
                var dataset = [];
                var today = new Date();
                var daysSorted = [];
                for (var i = 0; i < 15; i++) {
                    var newDate = new Date(today.setDate(today.getDate() - 1));
                    daysSorted.push(newDate);
                }
                for (let i = 0; i < result.length; i++) {
                    for (let j = 0; j < daysSorted.length; j++) {
                        if (result[i]._id.day == daysSorted[j].getDate()) {
                            if (dataset[j] == 0) {
                                dataset[j] = result[i].count
                            } else {
                                dataset.push(result[i].count)
                            }
                        } else {
                            if (dataset[j] == 0) {
                            } else {
                                if (dataset[j] > 0) {

                                } else {
                                    dataset.push(0);
                                }
                            }
                        }
                    }
                }
                console.log('15 facebook dataset2 >', dataset)

                resolve(dataset);
                client.close()
            })
        })
    })
}


function googlegGetLast15DaysReviews(email, day) {
    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, (err, client) => {
            const col = client.db(dbName).collection('googles')
            col.aggregate([
                {
                    $match: { email: email }
                },
                { $unwind: "$reviews" },
                {
                    $project: {
                        reviews: {
                            $dateFromString: {
                                dateString: '$reviews.createTime'
                            }
                        }
                    }
                },
                {
                    $match: {
                        reviews: { '$gte': new Date((new Date().getTime() - (15 * 24 * 60 * 60 * 1000))) }
                    }
                },
                {
                    $group: {
                        _id: {
                            day: { $dayOfMonth: "$reviews" }
                        }, count: { $sum: 1 }
                    }
                }

            ]).toArray((err, result) => {
                var dataset = [];
                var today = new Date();
                var daysSorted = [];
                for (var i = 0; i < 15; i++) {
                    var newDate = new Date(today.setDate(today.getDate() - 1));
                    daysSorted.push(newDate);
                }

                for (let i = 0; i < result.length; i++) {
                    for (let j = 0; j < daysSorted.length; j++) {
                        if (result[i]._id.day == daysSorted[j].getDate()) {
                            if (dataset[j] == 0) {
                                dataset[j] = result[i].count
                            } else {
                                dataset.push(result[i].count)
                            }
                        } else {
                            if (dataset[j] == 0) {
                            } else {
                                if (dataset[j] > 0) {

                                } else {
                                    dataset.push(0);
                                }
                            }
                        }
                    }
                }
                console.log('15 google dataset2 >', dataset)

                resolve(dataset);
                client.close()
            })
        })
    })
}


const getLast15DaysReviews = (req, res) => {
    var chartResponse = [];
    facebookGetLast15DaysReviews(req.body.email, req.body.day).then(faceBookData => {
        console.log('facebook >>>>', faceBookData)
        if (faceBookData == null) {
            chartResponse.push({
                platform: 'Facebook',
                data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                show: false
            })
        } else {
            chartResponse.push({
                platform: 'Facebook',
                data: faceBookData,
                show: true
            })
        }
        googlegGetLast15DaysReviews(req.body.email, req.body.day).then(googleData => {
            console.log('Google >>>', googleData)
            if (googleData == null) {
                chartResponse.push({
                    platform: 'Google',
                    data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    show: false
                })
            } else {
                chartResponse.push({
                    platform: 'Google',
                    data: googleData,
                    show: true
                })
                res.send({ data: chartResponse })
            }
        })
    })
}




//.....................getAnalyticsDataBYDays...................................


function FacebookAnalyticsData(email, day) {
    console.log('hit for 15 day');
    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, (err, client) => {
            const col = client.db(dbName).collection('facebooks');
            col.aggregate([
                {
                    $match: { email: email },

                },
                { $unwind: "$reviews" },
                {
                    $project: {
                        reviews: {
                            $dateFromString: {
                                dateString: '$reviews.created_time'
                            }
                        },

                    }
                },
                {
                    $match: {
                        reviews: { '$gte': new Date((new Date().getTime() - (day * 24 * 60 * 60 * 1000))) }
                    }
                },

                {
                    $group: {
                        _id: {
                            day: { $dayOfMonth: "$reviews" }
                        }, count: { $sum: 1 }

                    }
                }
            ]).toArray((err, result) => {
                var dataset = [];
                var today = new Date();
                var daysSorted = [];
                for (var i = 0; i < day; i++) {
                    var newDate = new Date(today.setDate(today.getDate() - 1));
                    daysSorted.push(newDate);
                }
                for (let i = 0; i < result.length; i++) {
                    for (let j = 0; j < daysSorted.length; j++) {
                        if (result[i]._id.day == daysSorted[j].getDate()) {
                            if (dataset[j] == 0) {
                                dataset[j] = result[i].count
                            } else {
                                dataset.push(result[i].count)
                            }
                        } else {
                            if (dataset[j] == 0) {
                            } else {
                                if (dataset[j] > 0) {

                                } else {
                                    dataset.push(0);
                                }
                            }
                        }
                    }
                }
                console.log(day + ' facebook dataset2 >', dataset)

                resolve(dataset);
                client.close()
            })
        })
    })
}



function googlegAnalyticsData(email, day) {
    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, (err, client) => {
            const col = client.db(dbName).collection('googles')
            col.aggregate([
                {
                    $match: { email: email }
                },
                { $unwind: "$reviews" },
                {
                    $project: {
                        reviews: {
                            $dateFromString: {
                                dateString: '$reviews.createTime'
                            }
                        }
                    }
                },
                {
                    $match: {
                        reviews: { '$gte': new Date((new Date().getTime() - (day * 24 * 60 * 60 * 1000))) }
                    }
                },
                {
                    $group: {
                        _id: {
                            day: { $dayOfMonth: "$reviews" }
                        }, count: { $sum: 1 }
                    }
                }

            ]).toArray((err, result) => {
                var dataset = [];
                var today = new Date();
                var daysSorted = [];
                for (var i = 0; i < day; i++) {
                    var newDate = new Date(today.setDate(today.getDate() - 1));
                    daysSorted.push(newDate);
                }
                
                if (result.length > 0) {
                    for (let i = 0; i < result.length; i++) {
                        for (let j = 0; j < daysSorted.length; j++) {
                            if (result[i]._id.day == daysSorted[j].getDate()) {
                                if (dataset[j] == 0) {
                                    dataset[j] = result[i].count
                                } else {
                                    dataset.push(result[i].count)
                                }
                            } else {
                                if (dataset[j] == 0) {
                                } else {
                                    if (dataset[j] > 0) {

                                    } else {
                                        dataset.push(0);
                                    }
                                }
                            }
                        }
                    }
                    console.log(day + ' google dataset2 >', dataset)
                } else {
                    result = [];
                }
                resolve(dataset);
                client.close()
            })
        })
    })
}


const getAnalyticsDataByDay = (req, res) => {
    var chartResponse = [];
    FacebookAnalyticsData(req.params.email, req.params.day).then(faceBookData => {
        console.log('facebook >>>>', faceBookData.length)
        if (faceBookData.length == 0) {
            var emptyDataSet = [];
            for (var i = 0; i < req.params.day; i++) {
                emptyDataSet.push(0)
            }
            chartResponse.push({
                platform: 'Facebook',
                data: emptyDataSet,
                show: false
            })
        } else {
            chartResponse.push({
                platform: 'Facebook',
                data: faceBookData,
                show: true
            })
        }
    })
   
        googlegAnalyticsData(req.params.email, req.params.day).then(googleData => {
            console.log('Google >>>', googleData.length)
            if (googleData.length == 0) {
                var emptyDataSet = [];
                for (var i = 0; i < req.params.day; i++) {
                    emptyDataSet.push(0)
                }
                console.log('here is empty--');
                chartResponse.push({
                    platform: 'Google',
                    data: emptyDataSet,
                    show: false
                })
            } else {
                chartResponse.push({
                    platform: 'Google',
                    data: googleData,
                    show: true
                })
            }

    }).then(() => {
        console.log('ChartResponse >>', chartResponse)
        res.send({ data: chartResponse })
    })


}




module.exports = {
    post,
    get,
    getOne,
    getfeedback,
    feedbackreplied,
    feedBackUrl,
    getReviewbyyearmonth,
    getReviewsCountThisMonth,
    getLastSevenDayReviews,
    getLast15DaysReviews,
    getAnalyticsDataByDay

}
