const msgcreditModel = require('../_specview_models/_specview_msgcredit_model').msgcreditModel
const userModel = require('../_specview_models/_specview_user_model').userModel

const post = (req, res) => {
    var msgcreditmodel = new msgcreditModel()
    msgcreditmodel.email = req.body.email
    msgcreditmodel.msgcredit = req.body.msgcredit

    userModel.findOne({email:req.body.email},(err,user)=>{
        if(err){
            throw err
        }else if(!user){
            res.json({success:false , msg:'not found'})
        }else{
            
            msgcreditmodel.firstname=user.firstname
            msgcreditmodel.lastname=user.lastname
            msgcreditmodel.contactno=user.contactno
            
            msgcreditmodel.save((err) => {
                if (err) {
                    console.log(err)
                    throw err
                } else {
                    res.json({ success: true, msg: 'posted' })
                }
            })
        }
    })

}

const getCount = (req, res) => {
    msgcreditModel.aggregate(
        [
            {
                $group: {
                    _id: '$status',
                    status: { $sum: { $cond: [{ $eq: ['$status', false] }, 1, 0] } },
                }
            },
        ]
        , function (err, count) {
            if (err) {
                throw err
            } else {
                res.json({ data: count })
            }
        }
    )
}

const getalldate =(req,res)=>{
    msgcreditModel.find((err,data)=>{
        if (err){
            throw err
        }else if(!data){
            res.json({success:false , msg:"not found"})
        }else{
            res.json({success:true , msg:"found",data:data})
        }
    })
}

const updateCredit = (req,res)=>{
    console.log(req.body)
    userModel.findOneAndUpdate({email:req.body.email},{$inc:{msgcredit:req.body.msgcredit}},(err,udata)=>{
        if(err){
            throw err
        }
        else if(!udata){
            res.json({success:false , msg:"not found"})
        }
        else{
            msgcreditModel.findOneAndUpdate({_id:req.body._id},{$set:{status:true,approve_date:Date.now()}},(err,status)=>{
                if(err){
                    throw err
                }
                else if(!status){
                    res.json({success:false , msg:"try again"})
                }
                else{
                     res.json({success:true})
                }
            })
            
        }
    })
}
module.exports = {
    post,
    getCount,
    getalldate,
    updateCredit,
}