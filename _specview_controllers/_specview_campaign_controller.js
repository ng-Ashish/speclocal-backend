const campaignModel = require('../_specview_models/_specview_campaign_model').campaignModel
const config = require('../_specview_config/_specview_dbconfig');
const userModel = require('../_specview_models/_specview_user_model').userModel
const customerModel = require('../_specview_models/_specview_customer_model').customerModel
const campaigntemplate = require('../_specview_models/_specview_msgtemplates_model').msgtemplateModel
const msg91 = require('../msg91/msg91').msg91;

const post = (req, res) => {
    var campaign = new campaignModel();
    campaign.client_id = req.body.client_id;
    campaign.email = req.body.email;
    campaign.message = req.body.message;
    campaign.customers = req.body.customers;
    campaign.type = req.body.type;
console.log("req.body :",req.body)

    campaigntemplate.updateOne({_id:req.body.templateId},{$set:{messagebody:req.body.message}},(err,updateData)=>{
        if(err){
            throw err
        }else if(updateData==null){
            res.json({success:false , msg:"data not updated  "})
        }else{
            console.log("updateData :",updateData)
            // process.exit(1);
            userModel.findOne({ email: req.body.email }, (err, userData) => {
                if (err) {
                    throw err
                } else if (userData == null) {
                    res.json({ success: false, msg: "user not found" })
                } else {
                    campaign.save(function (err, result) {
                        if (err) {
                            res.json({ success: false, msg: "error" })
                        }
                        else {
                            console.log("userData :",userData,userData.greeting)                    
                            var cnt = 0;
                            if (req.body.type == 'Feedback') {                  //Send Link
                                result.customers.forEach(customer => {
                                    var message = req.body.greeting + ' ' + customer.name + ' ' + req.body.message + ' https://feedback.speclocal.com/feedback/servey/' + result.client_id + "/" + customer._id + "/" + result._id;
                                    msg91.sendMessage(customer.phoneno, config.msg91key,'4', userData.smsserviceid, customer.dialCode, message);
                                    cnt++;
                                    customerModel.findOneAndUpdate({ client_id: req.body.client_id, 'customers._id': customer._id }, { $set: { "customers.$.campaignmsg": true } }, (err, msg) => {
                                        if (err) { throw err }
                                        if (!msg) {
                                            res.json({ success: false, msg: "not update" })
                                        } else {
                                            console.log(msg)
                                        }
                                    })
                                    if (cnt == result.customers.length) {
                                        userModel.findOneAndUpdate({ email: req.body.email }, { $inc: { msgcredit: -Math.abs(result.customers.length) } }, (err, udata) => {
                                            if (err) {
                                                throw err
                                            } else if (!udata) {
                                                // console.log('data :', udata)
                                            } else {
                                                res.send({
                                                    success: true
                                                });
                                            }
                                        })
        
                                    }
                                });
                            }else {
                                //Don't Send Link        
                                result.customers.forEach((customer) => {
                                var message = req.body.greeting + " " + customer.name + ' ' + req.body.message;
                                    msg91.sendMessage(customer.phoneno, config.msg91key, userData.route, userData.smsserviceid, customer.dialCode, message);
                                    cnt++;
                                    customerModel.findOneAndUpdate({ client_id: req.body.client_id, 'customers._id': customer._id }, { $set: { "customers.$.campaignmsg": true } }, (err, msg) => {
                                        if (err) { throw err }
                                        if (!msg) {
                                            res.json({ success: false, msg: "not update" })
                                        } else {
        
                                        }
                                    })
                                    if (cnt == result.customers.length) {
                                        userModel.findOneAndUpdate({ email: req.body.email }, { $inc: { msgcredit: -Math.abs(result.customers.length) } }, (err, udata) => {
                                            if (err) {
                                                throw err
                                            } else if (!udata) {
                                                // console.log('data :', udata)
                                            } else {
        
                                                res.send({
                                                    success: true
                                                });
                                            }
                                        })
        
                                    }
                                });
                            }
        
                        }
                    })
        
                }
            })
        }
    })
    
}

const get = (req, res) => {
    campaignModel.find({ email: req.params.email }, function (err, data) {
console.log("guruji pune campaign data : " ,data)
        if (err) {
            res.json({ success: false, msg: "error" })
        }
        else if (!data) {
            res.json({ success: false, msg: "not found" })
        }
        else {
            res.json({ success: true, msg: "found", data: [data] })
        }
    })
}

const campaignByMessage = (req, res) => {
    campaignModel.findOne({ email: req.body.email }, "customers", (err, data) => {
        if (err) {
            throw err
        }
        else
            if (!data || data == null) {
                res.json({ success: false, msg: "not found" })
            } else {
                var msg91 = require("msg91")(config.msg91key, config.sender, config.transactionalroute);
                var cnt = 0;
                data.customers.forEach((customer, index) => {
                    msg91.send(customer.phoneno, "Hi " + customer.name + config.message + " link:https://speclocal.com/feedback/" + data.client_id + "/" + customer._id, function (err, response) {

                        cnt++;
                        if (cnt == data.customers.length) {
                            userModel.findOneAndUpdate({ email: req.body.email }, { $inc: { msgcredit: -Math.abs(data.customers.length) } }, (err, udata) => {
                                if (err) {
                                    throw err
                                } else if (!udata) {
                                    console.log('data :', udata)
                                    //res.json({success:false ,msg:"not found"})
                                } else {
                                    console.log('udata success', udata);
                                    res.send({
                                        success: true
                                    });
                                }
                            })

                        }
                    });
                });

            }
    })

}

module.exports = {
    post,
    get,
    campaignByMessage,
}
