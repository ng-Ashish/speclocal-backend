const issueModel = require('../_specview_models/_specview_issue_model').issueModel;
const nodemailer = require('nodemailer')

const post = (req, res) => {
    var issue = new issueModel();
    issue.email = req.body.email;
    issue.issue_related_to = req.body.issue_related_to;
    issue.issue_description = req.body.issue_description;

    issue.save(function (err) {
        if (err) {
            res.json({ success: false, msg: "Error" })
        }
        else {
            res.json({ success: true, msg: "posted" })
        }
    })
}

const getAll = (req, res) => {
    issueModel.find(function (err, data) {
        if (err) {
            res.json({ success: false, msg: "Error" })
        }
        else if (!data || data == null) {
            res.json({ success: true, msg: "not found" })
        }
        else {
            res.json({ success: true, msg: "found", data: data })
        }
    })
}

const get = (req, res) => {
    issueModel.findOne({ email: req.params.email }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: "error" })
        }
        else if (!data || data == null) {
            res.json({ success: false, msg: "not found" })
        }
        else {
            res.json({ success: true, msg: "found", data: data })
        }
    })
}

//...............Mail Send..............................
const sendmail=(req,res)=>{
    var transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true, 
        auth: {
            user: 'sanketdeotarse09@gmail.com',
            pass: 'sanketoo7'
        },
            // use SSL    
    tls: {
        rejectUnauthorized: false
    }
     
});
    var mailOptions = {
        from:req.body.email ,
        to:'support@speclocal.com',
        subject:  "Issue Related_To:" + req.body.issue_related_to,      
        html: "Issue Description: <br>"  + req.body.issue_description,                   
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
		console.log(error.message);
            return res.json({ status: 'true', msg: error.message });
        } else {
            console.log('Email sent: ' + info.response);
            return res.json({ success: true, msg: "Mail Send sucessfully" });
        }
    });
}

module.exports = {
    post,
    getAll,
    get,
    sendmail,
    
}