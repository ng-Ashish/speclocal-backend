const employeeRequest = require('../_specview_models/_specview_employeerequest_model').employeerequestModel
const userModel = require('../_specview_models/_specview_user_model').userModel

const employeerequest =(req,res)=>{
    var request = new employeeRequest()
    request.email=req.body.email;
    request.noOfEmployees=req.body.noOfEmployees;

    userModel.findOne({email:req.body.email},(err,user)=>{
        if (err){
            throw err
        }else if(user==null){
            res.json({success:false , msg:"user not found"})
        }else{
            request.firstname=user.firstname;
            request.lastname=user.lastname;
            request.contactno=user.contactno;

            request.save((err,result)=>{
                if(err){
                    throw err
                }else{
                    res.json({success:true ,msg:"Request posted"})
                }
            })
        }
    })
}

const updateRequest=(req,res)=>{
    userModel.findOneAndUpdate({email:req.body.email},{$set:{noOfEmployees:req.body.noOfEmployees}},(err,udata)=>{
        if(err){
            throw err
        }else if(udata==null){
            res.json({success:false , msg:"not update"})
        }else{
            employeeRequest.findOneAndUpdate({email:req.body.email},{$set:{status:true,approve_date:Date.now()}},(err,status)=>{
                if(err){
                    throw err
                }else if(!status){
                    res.json({success:false , msg:"try again"})
                }else{
                    res.json({success:true})
                }
            })
        }
    })
}

const getAllRequest=(req,res)=>{
    employeeRequest.find({},(err,AllRequest)=>{
        if(err){
            throw err
        }else if(AllRequest==null){
            res.json({success:false , msg:"not found"})
        }else{
            res.json({success:true , msg:"Request Found",data:AllRequest})
        }
    })
}
module.exports={
    employeerequest,
    updateRequest,
    getAllRequest
}