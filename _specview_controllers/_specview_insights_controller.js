const insightModel = require('../_specview_models/_specview_insights_model').insightModel

const postInsightData = (req, res) => {
    var insight = new insightModel()
    insight.email = req.body.email
    insight.platform = req.body.platform
    insight.insight = req.body.insight

    insightModel.countDocuments({ email: req.body.email }, (err, data) => {
        if (err) {
            throw err
        } else if (data) {
            res.json({ success: false, Available: true })
        } else {
            insight.save((err, result) => {
                if (err) {
                    throw err
                } else {
                    res.json({ success: true, Available:false })
                }
            })
        }

    })
}

const getOneInsightData = (req, res) => {
    insightModel.find({ email: req.params.email }, (err, data) => {
        if (err) {
            throw err
        } else if (data == null) {
            res.json({ success: false, msg: "Record not Found" })
        } else {
            res.json({ success: true, msg: "Record Found", data: data })
        }
    })
}

const getInsightData = (req, res) => {
    insightModel.find({}, (err, data) => {
        if (err) {
            throw err
        } else if (data == null) {
            res.json({ success: false, msg: "Record not Found" })
        } else {
            res.json({ success: true, msg: "Record found", data: data })
        }
    })
}

module.exports = {
    postInsightData,
    getOneInsightData,
    getInsightData
}