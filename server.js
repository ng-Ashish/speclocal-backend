const express = require('express')
const bodyparser = require('body-parser')
const cors = require('cors')
const mongoose = require('mongoose')
const multer = require('multer')
const csv = require('csvtojson')
const app = express()
const dbconfig = require('./_specview_config/_specview_dbconfig')
const cornfile = require('./cron/cron').cronjobs
const port = 4500

const appAuthRoutes = require('./_specview_routes/_specview_auth_routes');
const userAuthRoutes = require('./_specview_routes/_specview_user_route');
const customerAuthRoutes = require('./_specview_routes/_specview_customer_route');
const zomatoRoutes = require('./_specview_routes/_specview_zomato_route');
const googleRoutes = require('./_specview_routes/_specview_google_route');
const facebookRoutes = require('./_specview_routes/_specview_facebook_route')
const issueRoutes = require('./_specview_routes/_specview_issue_route');
const campaignRoutes = require('./_specview_routes/_specview_campaign_route');
const feedbackRoutes = require('./_specview_routes/_specview_feedback_route');
const invitationRoutes = require('./_specview_routes/_specview_invitation_route');
const msgcreditRoutes = require('./_specview_routes/_specview_msgcredit_route');
const enquiryRoutes = require('./_specview_routes/_specview_enquiry_route');
const contactUsRoutes = require('./_specview_routes/_specview_contactUs_route');
const templateMessageRoutes = require('./_specview_routes/_specview_msgtemplate_route');
const serviceTypeRoutes = require('./_specview_routes/_specview_servicetype_routes');
const platformRoutes = require('./_specview_routes/_specview_platform_routes');
const smsServiceRoutes = require('./_specview_routes/_specview_smsService_routes');
const feedbackformRoutes = require('./_specview_routes/_specview_feedbackform_routes');
const employeerequest = require('./_specview_routes/_specview_employeerequest_route');
const insights = require('./_specview_routes/_specview_insights_route');

const https = require('https')
const fs = require('fs')
const path = require('path')
mongoose.Promise = global.Promise;
mongoose.connect(dbconfig.url, { useNewUrlParser: true }, (err) => {
    if (err) {
        console.log(err)
    } else {
        console.log('connected to db')
    }
})


app.use(bodyparser.json({limit: '1000mb'}))
app.use(bodyparser.urlencoded({ limit: '1000mb', extended: true }))
app.use(cors({ credentials: true, origin: true }))
//app.use('/', express.static(path.join(__dirname, 'images/')))
app.use('/auth', appAuthRoutes)
app.use('/user', userAuthRoutes)
app.use('/customer', customerAuthRoutes)
app.use('/zomato', zomatoRoutes)
app.use('/google', googleRoutes)
app.use('/facebook', facebookRoutes)
app.use('/issue', issueRoutes)
app.use('/campaign', campaignRoutes)
app.use('/feedback', feedbackRoutes)
app.use('/invitation', invitationRoutes)
app.use('/msgcredit', msgcreditRoutes)
app.use('/enquiry',enquiryRoutes.router)
app.use('/contactus',contactUsRoutes)
app.use('/templatemessage',templateMessageRoutes)
app.use('/servicetype' , serviceTypeRoutes)
app.use('/platform', platformRoutes)
app.use('/feedbackform' , feedbackformRoutes)
app.use('/smsservice', smsServiceRoutes)
app.use('/employeerequest',employeerequest);
app.use('/insight',insights)

//...............File Upload........................
const DIR = './uploads';

let StorageConfig = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR);
    },

    filename: (req, file, cb) => {
        cb(null, file.originalname);
    }
});
let Upload = multer({ storage: StorageConfig });

app.post('/api/upload', Upload.single('file'), (req, res) => {

    var filename = req.file.filename

    const csvFilePath = DIR + '/' + filename
    csv()
        .fromFile(csvFilePath)
        .then((jsonObj) => {
            console.log(jsonObj);
            res.json({ data: jsonObj });
        })

})



var DIR2 = '../public_html/images';
let StorageConfig2 = multer.diskStorage({
    destination: (req,file, cb) =>{
        cb(null , DIR2);
    },
    filename: (req,file,cb) =>{
        cb(null, Date.now()+file.originalname);
    }
});

let upload2 = multer({storage:StorageConfig2});

app.post('/api/uploadimage',upload2.single('image'),(req,res)=>{
    var returnUrl = 'https://speclocal.com/images/'+req.file.filename;
    res.send(returnUrl)
})


// https.createServer({
//     key: fs.readFileSync('../certificate/server.key', 'utf8'),
//     cert: fs.readFileSync('../certificate/speclocal-cert.pem', 'utf8')
// }, app)
//     .listen(port, function () {
//         console.log('Example app listening on port 4500! Go to https://speclocal.com:4500/')
//     })

app.listen(port, () => {
   console.log('Server started on port no: ' + port)
})


