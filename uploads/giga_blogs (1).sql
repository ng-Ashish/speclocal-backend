-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 16, 2018 at 11:06 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `giga_blogs`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(5) NOT NULL,
  `blog_id` decimal(5,0) NOT NULL,
  `blog_name` text NOT NULL,
  `blog_category` varchar(15) NOT NULL,
  `blog_image_url` text NOT NULL,
  `blog_short_desc` text NOT NULL,
  `blog_detail_desc` text NOT NULL,
  `likes` decimal(10,0) NOT NULL DEFAULT '0',
  `comments` decimal(10,0) NOT NULL DEFAULT '0',
  `author` varchar(127) NOT NULL DEFAULT 'ADMIN',
  `blog_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `blog_id`, `blog_name`, `blog_category`, `blog_image_url`, `blog_short_desc`, `blog_detail_desc`, `likes`, `comments`, `author`, `blog_timestamp`) VALUES
(19, '7871', 'Trail Sceinve', 'science', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS0O5gnLDG7WxPAEMXXg6b0uT8qRbGcvFUatYJiBM87qN2JUtY', 'Lore, Ipsume denatsie lotrander feoil', 'Lore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoilLore, Ipsume denatsie lotrander feoil', '0', '0', '', '2018-12-16 20:46:14'),
(20, '9856', 'Sample Technical', 'technical', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRQBiPWNY7tz1463F6c6R-ehwxV-b6yt9kmuvvbrwZzttyVMhlD', 'Ipsum Lorence', 'Ipsum reonce john doe iorest gurneruop\r\nIpsum reonce john doe iorest gurneruopIpsum reonce john doe iorest gurneruopIpsum reonce john doe iorest gurneruopIpsum reonce john doe iorest gurneruopIpsum reonce john doe iorest gurneruop', '0', '0', 'ADMIN', '2018-12-16 20:53:24'),
(21, '9861', 'Sasmple Fashion', 'fashion', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSRCGR5-irxHNh3rWSRBmNzBFalADKATXEtjuOTeTqhlLQXrv__0A', 'Lorem Ipsum ', 'Lorem Ipsum santres lorep masurem tulipop', '0', '0', 'ADMIN', '2018-12-16 20:53:24'),
(22, '8230', 'Technical 3-1', 'technical', 'https://image.shutterstock.com/image-vector/payroll-expenses-salary-calculation-concept-260nw-782797126.jpg', 'Hey shorty', 'yhis is the third tech block for the contents.\nSo,\n I would like to add new contents.', '0', '0', 'ADMIN', '2018-12-16 21:48:54');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(5) NOT NULL,
  `blog_id` decimal(5,0) NOT NULL,
  `comments` text NOT NULL,
  `comment_user_name` varchar(125) NOT NULL,
  `comment_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(5) NOT NULL,
  `username` varchar(127) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` decimal(10,0) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `username`, `email`, `mobile`, `message`) VALUES
(1, 'Ganesh', 'ganesh.321bari@gmail.com', '8789621035', 'sdlnifdlzgmxlf;m');

-- --------------------------------------------------------

--
-- Table structure for table `user_data`
--

CREATE TABLE `user_data` (
  `id` int(5) NOT NULL,
  `email` varchar(125) NOT NULL,
  `mobile` decimal(10,0) NOT NULL,
  `password` varchar(125) NOT NULL,
  `username` varchar(125) NOT NULL,
  `role` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_data`
--

INSERT INTO `user_data` (`id`, `email`, `mobile`, `password`, `username`, `role`) VALUES
(1, 'ganesh@mail.com', '8796534494', 'asdfghjkl', 'abc', 'ADMIN'),
(2, 'ganesh2@mail.com', '8787878787', 'asdfg', 'gb', 'USER');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blog_id` (`blog_id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_data`
--
ALTER TABLE `user_data`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_data`
--
ALTER TABLE `user_data`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
